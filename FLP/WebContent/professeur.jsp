<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Acces Professeur</title>
</head>
<body>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ include file="../header.jsp"%>

	<jsp:useBean id="ues" class="java.util.HashSet" scope="session" />
	<h4>Liste des Ues</h4>

	<table id="ueList" class="table table-striped">
		<thread>
		<tr>
			<th>Ue</th>
			<th>Info/Gestion Note</th>
			<th>Info Etudiants</th>
		</tr>
		</thread>
		<tbody>
			<c:forEach var="ue" items="${ues}">
				<tr>
					<td>${ue.nom}</td>
					<td><a
						href="${pageContext.request.contextPath}/listNoteUe?ue=${ue.nom}">Note</a></td>
					<td><a
						href="${pageContext.request.contextPath}/listEtudiant?ue=${ue.nom}">Étudiants</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<form action="${pageContext.request.contextPath}/listEtudiant">
		<input type="submit" value="Lister tous les etudiants">
	</form>

	<%@ include file="../footer.jsp"%>
</body>
</html>