<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>A propos !</title>
</head>
<body>
	<%@ include file="../header.jsp"%>

	<p>Ce projet est le recoupement de tous les Travaux pratiques
		effectu� au sein du cours de Fiabilit� enseign� par M Lugiez sur le
		campus de Luminy.</p>
	<br />

	<p>Celui-ci a �t� r�alis� par : <br />- Alexandre LEMAIRE en tant que chef
		de projet et graphiste.<br />- Maria LOUKAH qui a d�velopp� les
		fonctionnalit�s de lapplication.<br />- Mickael BLANC est le responsable du
		test et d�bogage.<br />- Julien BARREAULT a r�alis� la structure des
		informations affich�es.</p>

	<p>Cette Application bas�es sur une base de donn�es permet la
		gestion des classes �tudiantes supervis�es par un professeur.</p>
	<br />


	<%@ include file="../footer.jsp"%>
</body>
</html> 