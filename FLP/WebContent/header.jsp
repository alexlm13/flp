<script
	src="${pageContext.request.contextPath}/styles/jquery-2.1.1.min.js"></script>
<script src="${pageContext.request.contextPath}/styles/bootstrap.js"></script>
<link href="${pageContext.request.contextPath}/styles/bootstrap.min.css"
	rel="stylesheet">
<p style="text-align: center">
	<img
		src="${pageContext.request.contextPath}/images/Aix-Marseille_University_logo.png"
		alt="Mountain View" style="width: 295px; height: 106px">
</p>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div id="menu" style="margin-left: 7%; margin-right: 7%">
	<nav class="navbar navbar-default" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand"
				href="${pageContext.request.contextPath}/index.jsp">Accueil</a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<c:if test="${session.role == 'professeur'}">
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#">Professeur <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="${pageContext.request.contextPath}/listUe">Mon
									profil</a></li>
						</ul></li>
				</c:if>
				<c:if test="${session.role == 'etudiant'}">
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#">�tudiant<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="${pageContext.request.contextPath}/ueEtudiant">Mon
									profil</a></li>
						</ul></li>
				</c:if>
				<c:if test="${session.role == 'administrateur'}">
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#">Administrateur<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="${pageContext.request.contextPath}/creeProf">Cr�er
									Professeur</a></li>
							<li><a
								href="${pageContext.request.contextPath}/creeEtudiant">Cr�er
									Etudiant</a></li>
							<li><a href="${pageContext.request.contextPath}/creeUe">Cr�er
									Ue</a></li>
							<li><a href="${pageContext.request.contextPath}/listProf">Lister
									les professeurs</a></li>
							<li><a
								href="${pageContext.request.contextPath}/listEtudiant">Lister
									les �tudiants</a></li>
							<li><a href="${pageContext.request.contextPath}/listUe">Lister
									les ues</a></li>
						</ul></li>
				</c:if>
				<li><a href="${pageContext.request.contextPath}/apropos.jsp">A propos</a></li>
				<li><c:if test="${session == null}">
						<a href="${pageContext.request.contextPath}/login?mode=logIn">Login</a>
					</c:if>
					<c:if test="${session != null}">
						<a href="${pageContext.request.contextPath}/login?mode=logOut">Log out</a>
					</c:if></li>
			</ul>
		</div>
	</nav>
</div>
<div class="panel panel-default"
	style="margin-left: 7%; margin-right: 7%">
	<div class="panel-heading">
		<div id="titleDoc"></div>
	</div>
	<div class="panel-body">