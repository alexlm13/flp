<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lister les Ues</title>
</head>
<body>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ include file="../header.jsp"%>

	<jsp:useBean id="ues" class="java.util.HashSet" scope="session" />
	<table class="table table-striped" id="aff_ue">
		<thread>
		<tr>
			<th>Nom</th>
			<th>Professeur</th>
			<c:if test="${session.role == 'administrateur'}">
				<th>Supprimer</th>
			</c:if>
		</tr>
		</thread>
		<tbody>
			<c:forEach var="ue" items="${ues}">
				<tr>
					<td>${ue.nom}</td>
					<c:if test="${ue.professeur == null}"><td>Aucun professeur attribu�</td></c:if>
					<c:if test="${ue.professeur != null}"><td>${ue.professeur.nom}</td></c:if>
					<c:if test="${session.role == 'administrateur'}">
						<td><a
							href="${pageContext.request.contextPath}/creeUe?mode=delete&ue=${ue.nom}">Supprimer</a></td>
					</c:if>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<%@ include file="../footer.jsp"%>
</body>
</html>