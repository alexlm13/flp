<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lister les Professeurs</title>
</head>
<body>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ include file="../header.jsp"%>

	<jsp:useBean id="professeurs" class="java.util.HashSet" scope="session" />
	<table class="table table-striped" id="aff_student">
		<thread>
		<tr>
			<th>Num�ro</th>
			<th>Nom</th>
			<th>Pr�nom</th>
			<c:if test="${session.role == 'administrateur'}">
				<th>Supprimer</th>
			</c:if>
		</tr>
		</thread>
		<tbody>
			<c:forEach var="prof" items="${professeurs}">
				<tr>
					<td>${prof.identifiant}</td>
					<td>${prof.nom}</td>
					<td>${prof.prenom}</td>
					<c:if test="${session.role == 'administrateur'}">
						<td><a
							href="${pageContext.request.contextPath}/creeProf?mode=delete&id=${prof.identifiant}">Supprimer</a></td>
					</c:if>
				</tr>
			</c:forEach>
		</tbody>
	</table>


	<%@ include file="../footer.jsp"%>
</body>
</html>