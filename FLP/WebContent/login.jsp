<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Connexion</title>
</head>
<body>
	<%@ include file="../header.jsp"%>
	<font color="red">${error}</font>
	<c:if test="${error!=null}">
		<c:set var="error" value="${null}"></c:set>
	</c:if>
	<form class="form-horizontal" action="login" method="post">
		<div class="form-group">
			<label class="col-sm-2 control-label">Role</label>
			<div class="col-sm-10" style="width: 40%">
				<SELECT size="1" name="role">
					<OPTION>etudiant
					<OPTION>professeur
					<OPTION>administrateur
				</SELECT>
				</td>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Username </label>
			<div class="col-sm-10" style="width: 40%">
				<input class="form-control" value="${etudiantError.numEtudiant}"
					name="login">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Password </label>
			<div class="col-sm-10" style="width: 40%">
				<input class="form-control" value="${etudiantError.nom}"
					type="password" name="pass">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button name="valide" type="submit" class="btn btn-default">Valider</button>
			</div>
		</div>
	</form>
	<!-- <form method="post" action="login">
		<table width="100%" cellpadding="3" cellspacing="1" border="0"
			class="forumline">
			<tr>
				<td class="cat">Log in</td>
			</tr>
			<tr>
				<td class="row1" align="center">

					<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td>
							<SELECT size="1" name="role">
								<OPTION>etudiant
								<OPTION>professeur
								<OPTION>administrateur
								</SELECT>
							</td>
							<td class="gensmall">Username:</td>
							<td><input class="post" type="text" name="login"
								size="10" /></td>
							<td class="gensmall">Password:</td>
							<td><input class="post" type="password" name="pass"
								size="10" maxlength="32" /></td>
							<td><input type="submit" class="mainoption" name="enter"
								value="Log in" /></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>
	-->
	<!-- ${session} -->
	<%@ include file="../footer.jsp"%>


</body>
</html>