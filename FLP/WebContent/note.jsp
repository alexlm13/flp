<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Note Etudiant</title>
</head>
<body>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ include file="../header.jsp"%>

	<jsp:useBean id="notes" class="java.util.HashSet" scope="session" />

	<table class="table table-striped">
		<thead>
			<tr>
				<th hidden="hidden"></th>
				<th hidden="hidden">Num�ro �tudiant</th>
				<th>�tudiant</th>
				<th>Note</th>
				<c:if test="${session.role == 'professeur'}">
					<th>Modification Note</th>
					<th></th>
				</c:if>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="note" items="${notes}">
				<tr>
					<td>${note.etudiant.nom}</td>
					<c:if test="${note.valeur == -1}"><td>Pas encore not�</td></c:if>
					<c:if test="${note.valeur != -1}"><td>${note.valeur}</td></c:if>
					<c:if test="${session.role == 'professeur'}">
						<td>
							<form action="${pageContext.request.contextPath}/editNote" method="post">
								<input height="0" width="0" hidden="hidden" name="nomUe" value="${note.ue.nom}" />
								<input height="0" width="0" hidden="hidden" name="etudiant" value="${note.etudiant.numEtudiant}" />
								<input height="0" width="0" name="note" />
								<input type="submit" value="modifier" />
							</form>
						</td>
					</c:if>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<%@ include file="../footer.jsp"%>
</body>
</html>