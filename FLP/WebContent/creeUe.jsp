<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cr�ation d'une ue</title>
</head>
<body>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ include file="../header.jsp"%>

	<jsp:useBean id="professeurs" class="java.util.HashSet" scope="session" />

	<form class="form-horizontal" method="post"
		action="${pageContext.request.contextPath}/creeUe">
		<div class="form-group">
			<label class="col-sm-2 control-label">Nom : </label>
			<div class="col-sm-10" style="width: 40%">
				<input class="form-control" value="${etudiantError.numEtudiant}"
					name="nom">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">Professeur : </label>
			<div class="col-sm-10" style="width: 40%">

				<select name="prof">
					<c:forEach var="prof" items="${professeurs}">
						<option value="${prof.identifiant}">${prof.nom}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button name="ajoute_ue" type="submit" class="btn btn-default">Ajouter</button>
			</div>
		</div>
	</form>

	<%@ include file="../footer.jsp"%>
</body>
</html>