<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Profil Etudiant</title>
</head>
<body>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
	<%@ include file="../header.jsp"%>

	<jsp:useBean id="ueAvecNoteEtudiant" class="java.util.HashMap"
		scope="session" />
	<jsp:useBean id="uesPasInscris" class="java.util.HashSet"
		scope="session" />

	<table id="ueList" class="table">
		<thread>
		<tr>
			<th>Ue</th>
			<th>Note</th>
		</tr>
		</thread>
		<tbody>
			<c:forEach var="ue" items="${ueAvecNoteEtudiant}">
				<c:if test="${ue.value != '-1'}">
					<tr>
						<td>${ue.key}</td>
						<td>${ue.value}</td>
					</tr>
				</c:if>
				<c:if test="${ue.value == '-1'}">
					<tr>
						<td>${ue.key}</td>
						<td>Pas encore not�</td>
					</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
	<label>S'inscrire dans une ue</label>
	<form class="form-horizontal" method="post"
		action="${pageContext.request.contextPath}/listUe">
		<label>Nom de l'ue : </label> <select name="nomUe">
			<c:forEach var="ue" items="${uesPasInscris}">
				<option value="${ue.nom}">${ue.nom}</option>
			</c:forEach>
		</select>
		<button name="inscrit" class="btn btn-default" type="submit">S'inscrire</button>
	</form>
	<%@ include file="../footer.jsp"%>
</body>
</html>