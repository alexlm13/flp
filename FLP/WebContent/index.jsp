<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bonjour !</title>
</head>
<body>
	<%@ include file="../header.jsp"%>

	<c:if test="${info == null}">
		<c:if test="${error == null}">
			<p>Bonjour et bienvenue sur la page d'accueil du projet de
				fiabilit� logicielle 1.</p>
			<p>
				Vous pouvez dans un premier temps vous connecter via le bouton en
				haut � droite "login" puis celon vos droits modifier/cr�er ou
				obtenir certaines informations.<br />
			</p>
			<p>Bonne navigation !</p>
		</c:if>
	</c:if>
	<c:if test="${error != null}">
		<font color="red">${error}</font>
		<c:set var="error" value="${null}"></c:set>
	</c:if>
	<c:if test="${info != null}">
		<font color="green">${info}</font>
		<c:set var="info" value="${null}"></c:set>
	</c:if>

	<%@ include file="../footer.jsp"%>
</body>
</html>