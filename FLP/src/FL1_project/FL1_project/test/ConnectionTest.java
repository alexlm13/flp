package FL1_project.FL1_project.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;

import FL1_project.FL1_project.domain.persistence.JdbcTools;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */
public class ConnectionTest {
	private JdbcTools j;
	
	/**
	 * teste si une connection est établie avec la base de données
	 */
	@Test
	public void testConnect() throws SQLException {
		j = new JdbcTools();
		j.init();
		Connection c = j.newConnection();
		
		assertNotNull(c);
		
		c.close();
	}
	
	/**
	 * teste si une la fermeture de connexion avec la base de données est bien
	 * effectuée
	 */
	@Test
	public void testDeconnect() throws SQLException {
		j = new JdbcTools();
		j.init();
		Connection c = j.newConnection();
		
		assertNotNull(c);
		
		c.close();
		assertTrue(c.isClosed());
	}
	
}
