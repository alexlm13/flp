package FL1_project.FL1_project.test;

import java.io.FileInputStream;

import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.domain.persistence.Identification;
import FL1_project.FL1_project.service.implantation.EtudiantService;
import FL1_project.FL1_project.service.specifications.IEtudiantService;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */
public class EtuServiceTest extends DBTestCase {
	private Identification id;
	private IEtudiantService etudServ;
	private AbstractApplicationContext context;
	
	public EtuServiceTest() {
		context = new ClassPathXmlApplicationContext("spring.xml");
		context.registerShutdownHook();
		id = context.getBean("IdentificationProgramme", Identification.class);
		System.setProperty(
				PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS,
				"com.mysql.jdbc.Driver");
		System.setProperty(
				PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL,
				id.getNomPilote() + ":" + id.getUrl());
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME,
				id.getIdentifiant());
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD,
				id.getMotDePasse());
	}
	
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream(
				"dbunit/initial.xml"));
	}
	
	protected DatabaseOperation getSetUpOperation() throws Exception {
		etudServ = new EtudiantService();
		return DatabaseOperation.CLEAN_INSERT;
		// return DatabaseOperation.REFRESH;
	}
	
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}
	
	/**
	 * teste le service de consultation des notes pour un étudiant, avec un
	 * étudiant à null
	 */
	@Test
	public void testConsulterNoteEtudiantNull() throws Exception {
		Note note1 = context.getBean("note1", Note.class);
		etudServ.setEtudiant(null);
		try {
			etudServ.consulterNote(note1.getUe().getNom());
			fail("Exception non déclenchée");
		} catch (Exception e) {
		}
	}
	
	/**
	 * teste le service de consultation des notes pour un étudiant, avec un nom
	 * d'UE à null
	 */
	@Test
	public void testConsulterNoteUeNull() throws Exception {
		/* ue null seulement */
		try {
			etudServ.consulterNote(null);
			fail("Exception non déclenchée");
		} catch (Exception e) {
		}
	}
	
	/**
	 * teste le service de consultation des notes pour un étudiant, vérifie si
	 * la note obtenue correspond aux données préexistantes
	 */
	@Test
	public void testConsulterNoteEtud1() throws Exception {
		Note note8 = context.getBean("note8", Note.class);
		Etudiant etud1 = context.getBean("etudiant1", Etudiant.class);
		etudServ.setEtudiant(etud1);
		
		double expected = note8.getValeur();
		double actual = etudServ.consulterNote(note8.getUe().getNom())
				.getValeur();
		
		assertTrue(expected == actual);
	}
	
	/**
	 * teste le service de consultation des notes pour un étudiant, vérifie si
	 * la note obtenue correspond aux données préexistantes
	 */
	@Test
	public void testConsulterNoteEtud2() throws Exception {
		Note note9 = context.getBean("note9", Note.class);
		Etudiant etud2 = context.getBean("etudiant2", Etudiant.class);
		etudServ.setEtudiant(etud2);
		
		double expected = note9.getValeur();
		double actual = etudServ.consulterNote(note9.getUe().getNom())
				.getValeur();
		
		assertTrue(expected == actual);
	}
}
