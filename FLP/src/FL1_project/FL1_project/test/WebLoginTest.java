package FL1_project.FL1_project.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 */

public class WebLoginTest {
	private String uriPageHtmlunit;
	private WebClient webClient;
	private HtmlPage page;
	
	public WebLoginTest() throws FailingHttpStatusCodeException,
			MalformedURLException, IOException {
		uriPageHtmlunit = "http://localhost:8080/FLP/login?mode=login";
		webClient = new WebClient();
		webClient.getOptions().setRedirectEnabled(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setUseInsecureSSL(true);
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		webClient.getCookieManager().setCookiesEnabled(true);
	}
	
	@Before
	public void setUp() throws FailingHttpStatusCodeException,
			MalformedURLException, IOException {
		new WebLoginTest();
		page = webClient.getPage(uriPageHtmlunit);
	}
	
	@After
	public void tearDown() throws IOException {
		try {
			page.getAnchorByHref("/FLP/login?mode=logOut").click();
		} catch (ElementNotFoundException e) {
			System.err.println("Logout impossible");
		}
	}
	
	/**
	 * vérifie si on se trouve sur une page de login
	 */
	@Test
	public void textPage() throws Exception {
		assertTrue(page.asText().contains("Connexion"));
	}
	
	/**
	 * teste la connexion d'un administrateur
	 */
	@Test
	public void adminLogin() throws Exception {
		HtmlForm form = page.getForms().get(0);
		HtmlOption option = form.getSelectByName("role").getOptionByValue(
				"administrateur");
		option.setSelected(true);
		form.getInputByName("pass").setValueAttribute("PasswordAdm");
		page = form.getButtonByName("valide").click();
		assertTrue(page.asText().contains("Administrateur"));
	}
	
	/**
	 * teste la connexion d'un étudiant
	 */
	@Test
	public void etudLogin() throws Exception {
		HtmlForm form = page.getForms().get(0);
		HtmlOption option = form.getSelectByName("role").getOptionByValue(
				"etudiant");
		option.setSelected(true);
		form.getInputByName("login").setValueAttribute("2");
		form.getInputByName("pass").setValueAttribute("703");
		page = form.getButtonByName("valide").click();
		assertTrue(page.asText().contains("Étudiant"));
	}
	
	/**
	 * teste la connexion d'un professeur
	 */
	@Test
	public void profLogin() throws Exception {
		HtmlForm form = page.getForms().get(0);
		HtmlOption option = form.getSelectByName("role").getOptionByValue(
				"professeur");
		option.setSelected(true);
		form.getInputByName("login").setValueAttribute("1");
		form.getInputByName("pass").setValueAttribute("624");
		page = form.getButtonByName("valide").click();
		assertTrue(page.asText().contains("Professeur"));
	}
}
