package FL1_project.FL1_project.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 */

public class WebAdminTest {
	private String uriPageHtmlunit;
	private WebClient webClient;
	private HtmlPage page;
	
	public WebAdminTest() throws FailingHttpStatusCodeException,
			MalformedURLException, IOException {
		uriPageHtmlunit = "http://localhost:8080/FLP/login?mode=login";
		webClient = new WebClient();
		webClient.getOptions().setRedirectEnabled(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setUseInsecureSSL(true);
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		webClient.getCookieManager().setCookiesEnabled(true);
	}
	
	@Before
	public void setUp() throws FailingHttpStatusCodeException,
			MalformedURLException, IOException {
		new WebAdminTest();
		page = webClient.getPage(uriPageHtmlunit);
	}
	
	@After
	public void tearDown() throws IOException {
		try {
			page.getAnchorByHref("/FLP/login?mode=logOut").click();
		} catch (ElementNotFoundException e) {
			System.err.println("Logout impossible");
		}
	}
	
	/**
	 * login pour un administrateur
	 * 
	 * @throws Exception
	 */
	public void adminLogin() throws Exception {
		HtmlForm form = page.getForms().get(0);
		HtmlOption option = form.getSelectByName("role").getOptionByValue(
				"administrateur");
		option.setSelected(true);
		form.getInputByName("pass").setValueAttribute("PasswordAdm");
		page = form.getButtonByName("valide").click();
	}
	
	/**
	 * teste l'ajout (et la suppression) de professeur
	 */
	@Test
	public void ajouteProf() throws Exception {
		adminLogin();
		page = page.getAnchorByHref("/FLP/creeProf").click();
		HtmlForm form = page.getForms().get(0);
		form.getInputByName("nom").setValueAttribute("Mon");
		form.getInputByName("prenom").setValueAttribute("Prof");
		page = form.getButtonByName("ajoute_prof").click();
		assertTrue(page.asText().contains(
				"Le professeur Mon Prof a bien été créé."));
		
		/* supprimer le prof */
		HtmlPage delPage = null;
		page = page.getAnchorByHref("/FLP/listProf").click();
		HtmlTable table = (HtmlTable) page.getElementById("aff_student");
		assertNotNull(table);
		for (HtmlTableRow row : table.getRows()) {
			if (row.getCell(1).asText().equals("Mon")
					&& row.getCell(2).asText().equals("Prof")) {
				String numProf = row.getCell(0).asText();
				delPage = page.getAnchorByHref(
						"/FLP/creeProf?mode=delete&id=" + numProf).click();
				assertTrue(delPage.asText().contains(
						"Le professeur " + numProf + " a bien été supprimé."));
				
			}
		}
	}
	
	/**
	 * teste l'ajout (et la suppression) d'étudiant
	 */
	@Test
	public void ajouteEtud() throws Exception {
		adminLogin();
		page = page.getAnchorByHref("/FLP/creeEtudiant").click();
		HtmlForm form = page.getForms().get(0);
		form.getInputByName("nom").setValueAttribute("Mon");
		form.getInputByName("prenom").setValueAttribute("Etudiant");
		page = form.getButtonByName("ajoute_etud").click();
		assertTrue(page.asText().contains(
				"L'étudiant Mon Etudiant a bien été créé."));
		
		/* supprimer l'étudiant */
		HtmlPage delPage;
		page = page.getAnchorByHref("/FLP/listEtudiant").click();
		HtmlTable table = (HtmlTable) page.getElementById("aff_student");
		assertNotNull(table);
		for (HtmlTableRow row : table.getRows()) {
			System.err.println(row.getCell(1).asText() + " "
					+ row.getCell(2).asText());
			if (row.getCell(1).asText().equals("Mon")
					&& row.getCell(2).asText().equals("Etudiant")) {
				String numEtudiant = row.getCell(0).asText();
				delPage = page.getAnchorByHref(
						"/FLP/creeEtudiant?mode=delete&id=" + numEtudiant)
						.click();
				assertTrue(delPage.asText().contains(
						"L'étudiant " + numEtudiant + " a bien été supprimé."));
			}
		}
	}
	
	/**
	 * teste l'ajout (et la suppression) d'UE
	 */
	@Test
	public void ajouteUE() throws Exception {
		adminLogin();
		page = page.getAnchorByHref("/FLP/creeUe").click();
		HtmlForm form = page.getForms().get(0);
		form.getInputByName("nom").setValueAttribute("ID");
		HtmlOption option = form.getSelectByName("prof").getOptionByValue("2");
		option.setSelected(true);
		page = form.getButtonByName("ajoute_ue").click();
		assertTrue(page.asText().contains("L'ue ID a bien été créée."));
		
		/* supprimer l'UE */
		HtmlPage delPage;
		page = page.getAnchorByHref("/FLP/listUe").click();
		HtmlTable table = (HtmlTable) page.getElementById("aff_ue");
		assertNotNull(table);
		for (HtmlTableRow row : table.getRows()) {
			if (row.getCell(0).asText().equals("ID")
					&& row.getCell(1).asText().equals("Massat")) {
				delPage = page.getAnchorByHref("/FLP/creeUe?mode=delete&ue=ID")
						.click();
				assertTrue(delPage.asText().contains(
						"L'ue ID a bien été supprimée."));
			}
		}
	}
}
