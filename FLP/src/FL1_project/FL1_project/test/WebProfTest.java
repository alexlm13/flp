package FL1_project.FL1_project.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 */

public class WebProfTest {
	private String uriPageHtmlunit;
	private WebClient webClient;
	private HtmlPage page;
	
	public WebProfTest() throws FailingHttpStatusCodeException,
			MalformedURLException, IOException {
		uriPageHtmlunit = "http://localhost:8080/FLP/login?mode=login";
		webClient = new WebClient();
		webClient.getOptions().setRedirectEnabled(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setUseInsecureSSL(true);
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		webClient.getCookieManager().setCookiesEnabled(true);
	}
	
	@Before
	public void setUp() throws FailingHttpStatusCodeException,
			MalformedURLException, IOException {
		new WebProfTest();
		page = webClient.getPage(uriPageHtmlunit);
	}
	
	@After
	public void tearDown() throws IOException {
		try {
			page.getAnchorByHref("/FLP/login?mode=logOut").click();
		} catch (ElementNotFoundException e) {
			System.err.println("Logout impossible");
		}
	}
	
	/**
	 * login pour professeur
	 * 
	 * @throws Exception
	 */
	public void profLogin() throws Exception {
		HtmlForm form = page.getForms().get(0);
		HtmlOption option = form.getSelectByName("role").getOptionByValue(
				"professeur");
		option.setSelected(true);
		form.getInputByName("login").setValueAttribute("1");
		form.getInputByName("pass").setValueAttribute("624");
		page = form.getButtonByName("valide").click();
	}
	
	/**
	 * vérifie si le nombre de lignes du tableau des UEs
	 */
	@Test
	public void tableUERows() throws Exception {
		profLogin();
		page = page.getAnchorByHref("/FLP/listUe").click();
		HtmlTable table = (HtmlTable) page.getElementById("ueList");
		assertNotNull(table);
		assertEquals(2, table.getRowCount());
	}
	
	/**
	 * vérifie si le nom de l'UE présente dans le tableau
	 */
	@Test
	public void tableFL1() throws Exception {
		profLogin();
		page = page.getAnchorByHref("/FLP/listUe").click();
		HtmlTable table = (HtmlTable) page.getElementById("ueList");
		assertNotNull(table);
		assertEquals("FL1", table.getRow(1).getCell(0).asText());
	}
	
	/**
	 * vérifie si le nombre de lignes du tableau des étudiants de l'UE FL1
	 */
	@Test
	public void FL1EtudRows() throws Exception {
		profLogin();
		page = page.getAnchorByHref("/FLP/listUe").click();
		page = page.getAnchorByHref("/FLP/listEtudiant?ue=FL1").click();
		HtmlTable table = (HtmlTable) page.getElementById("aff_student");
		assertNotNull(table);
		assertEquals(4, table.getRowCount());
	}
	
	/**
	 * vérifie si le nom et le prénom de l'étudiant n°1
	 */
	@Test
	public void FL1Etud1() throws Exception {
		profLogin();
		page = page.getAnchorByHref("/FLP/listUe").click();
		page = page.getAnchorByHref("/FLP/listEtudiant?ue=FL1").click();
		HtmlTable table = (HtmlTable) page.getElementById("aff_student");
		assertNotNull(table);
		for (HtmlTableRow row : table.getRows()) {
			if (row.getCell(0).asText() == "1") {
				assertEquals("Barreault", row.getCell(1));
				assertEquals("Julien", row.getCell(2));
			}
		}
	}
	
	/**
	 * vérifie si la note de l'étudiant n°1
	 */
	@Test
	public void FL1Etud1Note() throws Exception {
		profLogin();
		page = page.getAnchorByHref("/FLP/listUe").click();
		page = page.getAnchorByHref("/FLP/listEtudiant?ue=FL1").click();
		HtmlTable table = (HtmlTable) page.getElementById("aff_student");
		assertNotNull(table);
		for (HtmlTableRow row : table.getRows()) {
			if (row.getCell(0).asText() == "Barreault") {
				assertEquals("9.0", row.getCell(1));
			}
		}
	}
	
	/**
	 * teste une modification invalide de la note de l'étudiant n°1
	 */
	@Test
	public void FL1Etud1NoteModifInvalide() throws Exception {
		profLogin();
		page = page.getAnchorByHref("/FLP/listUe").click();
		page = page.getAnchorByHref("/FLP/listEtudiant?ue=FL1").click();
		HtmlTable table = (HtmlTable) page.getElementById("aff_student");
		assertNotNull(table);
		for (HtmlTableRow row : table.getRows()) {
			if (row.getCell(0).asText() == "Barreault") {
				HtmlForm form = (HtmlForm) row.getCell(2)
						.getElementsByTagName("form").get(0);
				form.getInputByName("note").setValueAttribute("21");
				page = form.getInputByValue("modifier").click();
				assertTrue(page.asText().contains(
						"la note n'est pas comprise entre 0 et 20"));
			}
		}
	}
	
	/**
	 * teste une modification valide de la note de l'étudiant n°1
	 */
	@Test
	public void FL1Etud1NoteModifValide() throws Exception {
		profLogin();
		page = page.getAnchorByHref("/FLP/listUe").click();
		page = page.getAnchorByHref("/FLP/listEtudiant?ue=FL1").click();
		HtmlTable table = (HtmlTable) page.getElementById("aff_student");
		assertNotNull(table);
		for (HtmlTableRow row : table.getRows()) {
			if (row.getCell(0).asText() == "Barreault") {
				HtmlForm form = (HtmlForm) row.getCell(2)
						.getElementsByTagName("form").get(0);
				form.getInputByName("note").setValueAttribute("9");
				page = form.getInputByValue("modifier").click();
				assertTrue(page.asText().contains("La note a bien été changée"));
			}
		}
	}
}
