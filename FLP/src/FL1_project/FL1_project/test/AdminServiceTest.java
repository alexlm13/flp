package FL1_project.FL1_project.test;

import java.io.FileInputStream;
import java.sql.SQLException;

import org.dbunit.Assertion;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.domain.UE;
import FL1_project.FL1_project.domain.persistence.Identification;
import FL1_project.FL1_project.domain.persistence.NoteDao;
import FL1_project.FL1_project.service.implantation.AdministrateurService;
import FL1_project.FL1_project.service.implantation.ProfesseurService;
import FL1_project.FL1_project.service.specifications.IAdministrateurService;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */
public class AdminServiceTest extends DBTestCase {
	private Identification id;
	private IAdministrateurService adminServ;
	private ProfesseurService profServ;
	private AbstractApplicationContext context;
	
	public AdminServiceTest() {
		context = new ClassPathXmlApplicationContext("spring.xml");
		context.registerShutdownHook();
		id = context.getBean("IdentificationProgramme", Identification.class);
		System.setProperty(
				PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS,
				"com.mysql.jdbc.Driver");
		System.setProperty(
				PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL,
				id.getNomPilote() + ":" + id.getUrl());
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME,
				id.getIdentifiant());
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD,
				id.getMotDePasse());
	}
	
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream(
				"dbunit/initial.xml"));
	}
	
	protected DatabaseOperation getSetUpOperation() throws Exception {
		adminServ = new AdministrateurService();
		return DatabaseOperation.CLEAN_INSERT;
		// return DatabaseOperation.REFRESH;
	}
	
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}
	
	/**
	 * teste des tentatives invalides d'insertion de professeur
	 */
	@Test
	public void testProfNomInvalide() throws Exception {
		try {
			adminServ.creerProfesseur("", "Loukah");
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerProfesseur("maria", "");
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerProfesseur(null, "");
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerProfesseur("maria", null);
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerProfesseur("mar1ia", "dzaûdazd");
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerProfesseur("maria", "1dzaûdazd");
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Professeur");
		
		/* table attendue */
		IDataSet expectedDataSet = getDataSet(); // identique à la table
													// initiale
		ITable expectedTable = expectedDataSet.getTable("FL1_Professeur");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	/**
	 * vérifie si une entrée de professeur effectivement insérée contient les
	 * informations attendues
	 */
	@Test
	public void testProfRefValide() throws SQLException, Exception {
		assertNotNull(adminServ.creerProfesseur("Agopian", "Roland"));
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Professeur");
		
		/* table attendue */
		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new FileInputStream("dbunit/admin/expectedProf.xml"));
		ITable expectedTable = expectedDataSet.getTable("FL1_Professeur");
		
		/*
		 * les identifiants ne sont pas forcément les mêmes à cause de
		 * l'auto-incrémentation, on exclut donc la colonne des identifiants
		 * avant comparaison
		 */
		actualTable = DefaultColumnFilter.excludedColumnsTable(actualTable,
				new String[] { "identifiant" });
		expectedTable = DefaultColumnFilter.excludedColumnsTable(expectedTable,
				new String[] { "identifiant" });
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	/*
	 * teste la suppression de professeur
	 */
	@Test
	public void testProfSupprime() throws SQLException, Exception {
		Professeur prof1 = context.getBean("professeur1", Professeur.class);
		Professeur prof3 = context.getBean("professeur3", Professeur.class);
		
		/* suppression d'un professeur inexistant */
		adminServ.supprimerProfesseur(prof3.getIdentifiant());
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Professeur");
		
		/* table attendue */
		IDataSet expectedDataSet = getDataSet(); // identique à la table
													// initiale
		ITable expectedTable = expectedDataSet.getTable("FL1_Professeur");
		
		/* suppression d'un professeur existant */
		adminServ.supprimerProfesseur(prof1.getIdentifiant());
		
		/* table actuelle */
		databaseDataSet = getConnection().createDataSet();
		actualTable = databaseDataSet.getTable("FL1_Professeur");
		
		/* table attendue */
		expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new FileInputStream("dbunit/admin/expectedProfSuppr.xml"));
		expectedTable = expectedDataSet.getTable("FL1_Professeur");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	/**
	 * teste des tentatives invalides d'insertion d'étudiant
	 */
	@Test
	public void testEtudNomInvalide() throws SQLException, Exception {
		try {
			adminServ.creerEtudiant("", "Loukah");
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerEtudiant("maria", "");
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerEtudiant(null, "");
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerEtudiant("maria", null);
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerEtudiant("mar1ia", "dzaûdazd");
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerEtudiant("maria", "1dzaûdazd");
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Etudiant");
		
		/* table attendue */
		IDataSet expectedDataSet = getDataSet(); // identique à la table
													// initiale
		ITable expectedTable = expectedDataSet.getTable("FL1_Etudiant");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	/**
	 * vérifie si une entrée d'étudiant effectivement insérée contient les
	 * informations attendues
	 */
	@Test
	public void testEtudRefValide() throws SQLException, Exception {
		assertNotNull(adminServ.creerEtudiant("Blanc", "Michaël"));
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Etudiant");
		
		/* table attendue */
		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new FileInputStream("dbunit/admin/expectedEtud.xml"));
		ITable expectedTable = expectedDataSet.getTable("FL1_Etudiant");
		
		/* filtrage de la colonne de clé primaire */
		actualTable = DefaultColumnFilter.excludedColumnsTable(actualTable,
				new String[] { "numEtudiant" });
		expectedTable = DefaultColumnFilter.excludedColumnsTable(expectedTable,
				new String[] { "numEtudiant" });
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	/**
	 * teste la suppression d'étudiants
	 */
	@Test
	public void testEtudSupprime() throws SQLException, Exception {
		Etudiant etu1 = context.getBean("etudiant1", Etudiant.class);
		Etudiant etu4 = context.getBean("etudiant4", Etudiant.class);
		
		/* suppression d'un étudiant inexistant */
		adminServ.supprimerEtudiant(etu4.getNumEtudiant());
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Etudiant");
		
		/* table attendue */
		IDataSet expectedDataSet = getDataSet(); // identique à la table
													// initiale
		ITable expectedTable = expectedDataSet.getTable("FL1_Etudiant");
		
		/* suppression d'un étudiant existant */
		adminServ.supprimerEtudiant(etu1.getNumEtudiant());
		
		/* table actuelle */
		databaseDataSet = getConnection().createDataSet();
		actualTable = databaseDataSet.getTable("FL1_Etudiant");
		
		/* table attendue */
		expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new FileInputStream("dbunit/admin/expectedEtudSuppr.xml"));
		expectedTable = expectedDataSet.getTable("FL1_Etudiant");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	/**
	 * teste des tentatives invalides d'insertion d'UE
	 */
	@Test
	public void testUEInvalide() throws SQLException, Exception {
		try {
			adminServ.creerUE("", null);
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			adminServ.creerUE(null, null);
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Ue");
		
		/* table attendue */
		IDataSet expectedDataSet = getDataSet(); // identique à la table
													// initiale
		ITable expectedTable = expectedDataSet.getTable("FL1_Ue");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	/**
	 * teste une insertion d'UE sans professeur
	 */
	@Test
	public void testUESansProf() throws SQLException, Exception {
		UE ue = adminServ.creerUE("SH", null);
		assertNotNull(ue);
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Ue");
		
		/* table attendue */
		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new FileInputStream("dbunit/admin/expectedUE.xml"));
		ITable expectedTable = expectedDataSet.getTable("FL1_Ue");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	/**
	 * teste la suppression d'UE
	 */
	@Test
	public void testUESupprime() throws SQLException, Exception {
		UE ue1 = context.getBean("ue1", UE.class);
		UE ue2 = context.getBean("ue2", UE.class);
		
		/* suppression d'une UE inexistante */
		adminServ.supprimerUE(ue2.getNom());
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Ue");
		
		/* table attendue */
		IDataSet expectedDataSet = getDataSet(); // identique à la table
													// initiale
		ITable expectedTable = expectedDataSet.getTable("FL1_Ue");
		
		/* suppression d'une UE existante */
		adminServ.supprimerUE(ue1.getNom());
		
		/* table actuelle */
		databaseDataSet = getConnection().createDataSet();
		actualTable = databaseDataSet.getTable("FL1_Ue");
		
		/* table attendue */
		expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new FileInputStream("dbunit/admin/expectedUESuppr.xml"));
		expectedTable = expectedDataSet.getTable("FL1_Ue");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	/**
	 * teste des modifications valides de notes (le professeur doit en avoir le
	 * droit)
	 */
	@Test
	public void testModifierNote() throws SQLException, Exception {
		Professeur prof1 = context.getBean("professeur1", Professeur.class);
		Professeur prof2 = context.getBean("professeur2", Professeur.class);
		
		profServ = new ProfesseurService(prof1);
		NoteDao nDao = new NoteDao();
		Note noteOld = nDao.getNoteByUeAndStudent("FL1", 2);
		
		/* modifications invalides */
		try {
			profServ.modifierNoteEtudiant(noteOld, 20.1);
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		try {
			profServ.modifierNoteEtudiant(noteOld, -0.1);
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Note");
		
		/* table attendue */
		IDataSet expectedDataSet = getDataSet(); // identique à la table
													// initiale
		ITable expectedTable = expectedDataSet.getTable("FL1_Note");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
		
		/* modification valide */
		assertNotNull(profServ.modifierNoteEtudiant(noteOld, 12.0));
		
		/* table actuelle */
		databaseDataSet = getConnection().createDataSet();
		actualTable = databaseDataSet.getTable("FL1_Note");
		
		/* table attendue */
		expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new FileInputStream("dbunit/admin/expectedNote.xml"));
		expectedTable = expectedDataSet.getTable("FL1_Note");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
		
		/* on essaye de modifier cette note avec un professeur non autorisé */
		profServ = new ProfesseurService(prof2);
		try {
			profServ.modifierNoteEtudiant(noteOld, 10.0);
			fail("Exception non lancée");
		} catch (Exception e) {
		}
		
		/* table actuelle */
		databaseDataSet = getConnection().createDataSet();
		actualTable = databaseDataSet.getTable("FL1_Note");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
}
