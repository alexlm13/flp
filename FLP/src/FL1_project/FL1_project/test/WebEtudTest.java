package FL1_project.FL1_project.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import FL1_project.FL1_project.domain.persistence.JdbcTools;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 */

public class WebEtudTest {
	private String uriPageHtmlunit;
	private WebClient webClient;
	private HtmlPage page;
	
	public WebEtudTest() throws FailingHttpStatusCodeException,
			MalformedURLException, IOException {
		uriPageHtmlunit = "http://localhost:8080/FLP/login?mode=login";
		webClient = new WebClient();
		webClient.getOptions().setRedirectEnabled(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getOptions().setCssEnabled(false);
		webClient.getOptions().setUseInsecureSSL(true);
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		webClient.getCookieManager().setCookiesEnabled(true);
	}
	
	@Before
	public void setUp() throws FailingHttpStatusCodeException,
			MalformedURLException, IOException {
		new WebEtudTest();
		page = webClient.getPage(uriPageHtmlunit);
	}
	
	@After
	public void tearDown() throws IOException {
		try {
			page.getAnchorByHref("/FLP/login?mode=logOut").click();
		} catch (ElementNotFoundException e) {
			System.err.println("Logout impossible");
		}
	}
	
	/**
	 * login pour étudiant
	 * 
	 * @throws Exception
	 */
	public void etudLogin() throws Exception {
		HtmlForm form = page.getForms().get(0);
		HtmlOption option = form.getSelectByName("role").getOptionByValue(
				"etudiant");
		option.setSelected(true);
		form.getInputByName("login").setValueAttribute("2");
		form.getInputByName("pass").setValueAttribute("703");
		page = form.getButtonByName("valide").click();
	}
	
	/**
	 * vérifie la note de l'étudiant courant en FL1
	 */
	@Test
	public void EtudNoteFL1() throws Exception {
		etudLogin();
		page = page.getAnchorByHref("/FLP/ueEtudiant").click();
		HtmlTable table = (HtmlTable) page.getElementById("ueList");
		assertNotNull(table);
		for (HtmlTableRow row : table.getRows()) {
			if (row.getCell(0).asText() == "FL1") {
				assertEquals("12", row.getCell(1));
			}
		}
	}
	
	/**
	 * teste l'inscription d'un étudiant dans l'UE XML
	 */
	@Test
	public void EtudInscriptionXML() throws Exception {
		etudLogin();
		page = page.getAnchorByHref("/FLP/ueEtudiant").click();
		HtmlForm form = page.getForms().get(0);
		HtmlOption option = form.getSelectByName("nomUe").getOptionByValue(
				"XML");
		option.setSelected(true);
		page = form.getButtonByName("inscrit").click();
		assertTrue(page.asText().contains(
				"Vous vous êtes bien inscrit à l'ue XML"));
		
		/*
		 * supprimer l'inscription : l'étudiant n'y a pas directement accès,
		 * c'est l'administrateur qui s'en charge
		 */
		JdbcTools j = new JdbcTools();
		j.init();
		j.executeUpdate("DELETE FROM FL1_UesEtudiants WHERE nomUE='XML' AND numEtudiant=2");
		j.close();
	}
}
