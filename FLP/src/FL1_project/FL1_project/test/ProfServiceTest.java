package FL1_project.FL1_project.test;

import java.io.FileInputStream;

import org.dbunit.Assertion;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Test;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.domain.UE;
import FL1_project.FL1_project.domain.persistence.Identification;
import FL1_project.FL1_project.service.implantation.ProfesseurService;
import FL1_project.FL1_project.service.specifications.IProfesseurService;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */
public class ProfServiceTest extends DBTestCase {
	private Identification id;
	private IProfesseurService profServ;
	private AbstractApplicationContext context;
	
	public ProfServiceTest() {
		context = new ClassPathXmlApplicationContext("spring.xml");
		context.registerShutdownHook();
		id = context.getBean("IdentificationProgramme", Identification.class);
		System.setProperty(
				PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS,
				"com.mysql.jdbc.Driver");
		System.setProperty(
				PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL,
				id.getNomPilote() + ":" + id.getUrl());
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME,
				id.getIdentifiant());
		System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD,
				id.getMotDePasse());
	}
	
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new FileInputStream(
				"dbunit/initial.xml"));
	}
	
	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.CLEAN_INSERT;
		// return DatabaseOperation.REFRESH;
	}
	
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}
	
	/*
	 * teste différentes insertions de notes, valides ou non, sachant que seul
	 * un professeur enseignant dans l'UE peut modifier ces notes
	 */
	@Test
	public void testModifierNoteNull() throws Exception {
		Professeur prof1 = context.getBean("professeur1", Professeur.class);
		profServ = new ProfesseurService(prof1);
		
		try {
			profServ.modifierNoteEtudiant(null, 0);
			fail("Exception non déclenchée");
		} catch (Exception e) {
		}
	}
	
	@Test
	public void testModifierNoteParfaite() throws Exception {
		Note note8 = context.getBean("note8", Note.class);
		Professeur prof1 = context.getBean("professeur1", Professeur.class);
		profServ = new ProfesseurService(prof1);
		
		profServ.modifierNoteEtudiant(note8, 20.0);
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Note");
		
		/* table attendue */
		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new FileInputStream("dbunit/prof/expectedParfait.xml"));
		ITable expectedTable = expectedDataSet.getTable("FL1_Note");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	@Test
	public void testModifierNoteZero() throws Exception {
		Note note8 = context.getBean("note8", Note.class);
		Professeur prof1 = context.getBean("professeur1", Professeur.class);
		profServ = new ProfesseurService(prof1);
		
		profServ.modifierNoteEtudiant(note8, 0.0);
		
		/* table actuelle */
		IDataSet databaseDataSet = getConnection().createDataSet();
		ITable actualTable = databaseDataSet.getTable("FL1_Note");
		
		/* table attendue */
		IDataSet expectedDataSet = new FlatXmlDataSetBuilder()
				.build(new FileInputStream("dbunit/prof/expectedZero.xml"));
		ITable expectedTable = expectedDataSet.getTable("FL1_Note");
		
		/* comparaison */
		Assertion.assertEquals(expectedTable, actualTable);
	}
	
	@Test
	public void testModifierNoteInvalideSup() throws Exception {
		Note note8 = context.getBean("note8", Note.class);
		Professeur prof1 = context.getBean("professeur1", Professeur.class);
		profServ = new ProfesseurService(prof1);
		
		try {
			profServ.modifierNoteEtudiant(note8, 20.1);
			fail("Exception non déclenchée");
		} catch (Exception e) {
		}
	}
	
	@Test
	public void testModifierNoteInvalideInf() throws Exception {
		Note note8 = context.getBean("note8", Note.class);
		Professeur prof1 = context.getBean("professeur1", Professeur.class);
		profServ = new ProfesseurService(prof1);
		
		try {
			profServ.modifierNoteEtudiant(note8, -0.1);
			fail("Exception non déclenchée");
		} catch (Exception e) {
		}
	}
	
	@Test
	public void testModifierNoteProfNonAutorise() throws Exception {
		Note note8 = context.getBean("note8", Note.class);
		Professeur prof2 = context.getBean("professeur2", Professeur.class);
		profServ = new ProfesseurService(prof2);
		
		try {
			/* on essaye de modifier cette note avec un professeur non autorisé */
			profServ.modifierNoteEtudiant(note8, 10.0);
			fail("Exception non déclenchée");
		} catch (Exception e) {
		}
	}
	
	/*
	 * dans le cas normal, un professeur enseignant dans une UE doit obtenir
	 * toutes les notes de cette UE
	 */
	@Test
	public void testConsulterToutesNotesNull() throws Exception {
		Professeur prof1 = context.getBean("professeur1", Professeur.class);
		profServ = new ProfesseurService(prof1);
		
		try {
			/* cas null */
			profServ.consulterToutesNotes(null);
			fail("Exception non déclenchée");
		} catch (Exception e) {
		}
	}
	
	@Test
	public void testConsulterToutesNotes() throws Exception {
		UE ue1 = context.getBean("ue1", UE.class);
		Professeur prof1 = context.getBean("professeur1", Professeur.class);
		profServ = new ProfesseurService(prof1);
		
		/* cas normal */
		profServ.consulterToutesNotes(ue1.getNom());
	}
}