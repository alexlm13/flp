package FL1_project.FL1_project.service.implantation;

import java.util.Collection;
import java.util.Map;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.domain.UE;
import FL1_project.FL1_project.domain.persistence.EtudiantDao;
import FL1_project.FL1_project.domain.persistence.NoteDao;
import FL1_project.FL1_project.domain.persistence.UEDao;
import FL1_project.FL1_project.service.specifications.IEtudiantService;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette class permet à un étudiant (dont la class possède une instance)
 *         de consulter sa note d'une ue.
 */

public class EtudiantService implements IEtudiantService {
	Etudiant etudiant;
	NoteDao noteDao = new NoteDao();
	EtudiantDao etudiantDao = new EtudiantDao();
	
	/**
	 * La méthode consulterNote permet de consulter comme son nom l'indique la
	 * note de l'étudiant dont la class possède une instance.</br> Pour cette
	 * récupération la méthode à besoin d'informations concernant l'ue qui est
	 * passé en argument.</br>
	 * 
	 * @param ue
	 *            L'ue dont l'étudiant aimerait avoir sa note.</br>
	 * @return La méthode renvoit la note de l'étudiant si toutes les
	 *         spécifications ci-dessus sont corrects, sinon elle échoue et
	 *         renvoit null.</br>
	 * @throws Exception
	 */
	public Note consulterNote(String ue) throws Exception {
		if (ue == null || etudiant == null || etudiant.getNumEtudiant() < 0)
			throw new Exception("Des champs sont vides ou null.");
		return noteDao.getNoteByUeAndStudent(ue, etudiant.getNumEtudiant());
	}
	
	public Collection<Etudiant> getAllEtudiants() throws Exception {
		return etudiantDao.getAllEtudiants();
	}
	
	public Etudiant getEtudiant() {
		return etudiant;
	}
	
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	
	public void addEtudiantUe(Integer etu, String ue) throws Exception {
		new UEDao().insertUeEtu(etu, ue);
	}
	
	/*
	 * public static void main(String[] args) throws Exception {
	 * System.out.println("début"); new EtudiantService().TestaddEtuUE(); }
	 */
	
	public Etudiant etudiantById(Integer parameter) throws Exception {
		// TODO Auto-generated method stub
		return etudiantDao.getEtudiant(parameter);
	}
	
	public Collection<Etudiant> etudiantsByUe(String ue) throws Exception {
		// TODO Auto-generated method stub
		return etudiantDao.etudiantsByUe(ue);
	}
	
	public Map<String, String> notesByEtudiant(Integer id) throws Exception {
		// TODO Auto-generated method stub
		return noteDao.getNoteByStudent(id);
		
	}
	
	public Collection<UE> selectNotAssociateUEEtu(int numEtudiant)
			throws Exception {
		return new UEDao().selectNotAssociateUEEtu(numEtudiant);
	}
	
	public static void main(String[] args) throws Exception {
		Collection<UE> Ues = new EtudiantService().selectNotAssociateUEEtu(3);
		
		for (UE ue : Ues) {
			System.out.println(ue.getNom());
		}
		System.out.println(Ues.size());
	}
	
}
