package FL1_project.FL1_project.service.implantation;

import java.util.Collection;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.domain.UE;
import FL1_project.FL1_project.domain.persistence.EtudiantDao;
import FL1_project.FL1_project.domain.persistence.NoteDao;
import FL1_project.FL1_project.domain.persistence.ProfesseurDao;
import FL1_project.FL1_project.domain.persistence.UEDao;
import FL1_project.FL1_project.service.specifications.IProfesseurService;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette class permet de de modifier des notes des étudiants et de
 *         consulter toutes les notes d'une ue.
 */

public class ProfesseurService implements IProfesseurService {
	private Professeur professeur;
	NoteDao noteDao = new NoteDao();
	EtudiantDao etudiantDao = new EtudiantDao();
	UEDao ueDao = new UEDao();
	ProfesseurDao professeurDao = new ProfesseurDao();
	
	public ProfesseurService(Professeur professeur) {
		super();
		this.professeur = professeur;
	}
	
	public ProfesseurService() {
		super();
	}
	
	public Professeur getProfesseur() {
		return professeur;
	}
	
	public void setProfesseur(Professeur professeur) {
		this.professeur = professeur;
	}
	
	/**
	 * La méthode modifierNoteEtudiant permet de modifier comme son nom
	 * l'indique une note d'un étudiant.</br> Pour cette création la méthode à
	 * besoin d'informations concernant la note.</br>
	 * 
	 * @param note
	 *            La note de l'étudiant à modifier
	 * @param noteSet
	 *            La nouvelle note à modifier qui soit être comprise entre 0 et
	 *            20.
	 * @return La méthode renvoit la note modifiée passé en argument si elle
	 *         réussi, sinon elle échoue et renvoit null.</br>
	 * @throws Exception
	 */
	public Note modifierNoteEtudiant(Note note, double noteSet)
			throws Exception {
		if (note == null)
			throw new Exception("Des champs sont vides ou null.");
		if (noteSet < 0 || noteSet > 20)
			throw new Exception(
					"La note ne peut être inférieure à 0 ou supérieure à 20");
		
		if (!professeur.equals(note.getUe().getProfesseur()))
			throw new Exception(
					"Le professeur n'a pas le droit de modifier cette UE.");
		
		note.setValeur(noteSet);
		noteDao.update(note, note);
		return note;
	}
	
	/**
	 * La méthode consulterToutesNotes permet de consulter comme son nom
	 * l'indique toutes les notes d'une Ue.</br> Pour cette création la méthode
	 * à besoin d'informations concernant l'Ue.</br>
	 * 
	 * @param ue
	 *            L'ue dont on veut récupérer les notes
	 * @return La méthode renvoit une liste de notes si elle réussi, sinon elle
	 *         échoue et renvoit null.</br>
	 * @throws Exception
	 */
	public Collection<Note> consulterToutesNotes(String ue) throws Exception {
		// assert professeur.equals(ue.getProfesseur()); <= A voir si on rajoute
		// cette sécurité
		/* note de mika: je pense que oui, c'est dans les specs du projet */
		if (professeur == null || ue == null)
			throw new Exception("Des champs sont vides ou null.");
		// if(professeur.getIdentifiant() !=
		// ue.getProfesseur().getIdentifiant()) throw new
		// Exception("Le professeur n'a pas de droit de modifier cette UE.");
		
		return new NoteDao().getNotesUe(ue);
	}
	
	public Collection<UE> getUesOfProfesseur() throws Exception {
		return ueDao.getUesOfProfesseur(professeur.getIdentifiant());
	}
	
	@Override
	public Collection<Professeur> getAllProfesseur() throws Exception {
		return professeurDao.getAllProfesseur();
	}
	
	public Professeur professeurById(Integer integer) throws Exception {
		// TODO Auto-generated method stub
		return professeurDao.getProfesseur(integer);
	}
	
	public Collection<Etudiant> etudiantsByProfesseur(String login)
			throws Exception {
		// TODO Auto-generated method stub
		return etudiantDao.etudiantsByProfesseur(professeur.getIdentifiant());
	}
	
}
