package FL1_project.FL1_project.service.implantation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.domain.UE;
import FL1_project.FL1_project.service.specifications.IUECsvService;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public class UECsvService implements IUECsvService {
	/**
	 * La méthode construireFichier permet de consulter comme son nom l'indique
	 * la construction du fichier des ue.</br> Pour cette récupération la
	 * méthode à besoin d'informations concernant l'ue qui est passé en
	 * argument.</br>
	 * 
	 * @param ue
	 *            L'ue à sauvegarder dans le fichier. L'ue ne doit pas être
	 *            vide</br>
	 * @return La méthode renvoit true si toutes les spécifications ci-dessus
	 *         sont corrects, sinon elle échoue et renvoit false.</br>
	 */
	public boolean construireFichier(UE ue) {
		boolean state = true;
		
		if (ue == null)
			return false;
		FileWriter writer = null;
		String texte = "texte à insérer à la fin du fichier";
		Collection<Note> notes = ue.getNotes();
		System.out.println(notes.size());
		try {
			writer = new FileWriter("UE" + ue.getNom() + ".txt", false);
			for (Note note : notes) {
				System.out.println(note.getEtudiant().getNom() + ","
						+ note.getEtudiant().getPrenom() + ","
						+ note.getEtudiant().getNumEtudiant() + ","
						+ note.getValeur() + "\n");
				texte = note.getEtudiant().getNom() + ","
						+ note.getEtudiant().getPrenom() + ","
						+ note.getEtudiant().getNumEtudiant() + ","
						+ note.getValeur() + "\n";
				writer.write(texte, 0, texte.length());
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			state = false;
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return state;
		}
	}
	
	/**
	 * La méthode lireFichier permet de lire comme son nom l'indique un fichier
	 * d'une ue.</br> Pour cette récupération la méthode à besoin d'informations
	 * concernant les liens qui sont passé en argument.</br>
	 * 
	 * @param fileName
	 *            Le nom du fichier.</br>
	 * @return La méthode renvoit l'ue si toutes les spécifications ci-dessus
	 *         sont corrects, sinon elle échoue et renvoit false.</br>
	 */
	public UE lireFichier(String fileName) {
		if (fileName == null)
			return null;
		
		BufferedReader buff = null;
		String ueName = fileName.replace("UE", "");
		ueName = ueName.replace(".txt", "");
		try {
			buff = new BufferedReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			return null;
		}
		
		UE ue = new UE();
		ue.setNom(ueName);
		
		String line;
		String[] res;
		try {
			while ((line = buff.readLine()) != null) {
				res = line.split(",");
				if (res.length != 4) {
					buff.close();
					return null;
				}
				Etudiant etu = new Etudiant();
				etu.setNom(res[0]);
				etu.setPrenom(res[1]);
				try {
					etu.setNumEtudiant(new Integer(res[2]));
				} catch (NumberFormatException e) {
					buff.close();
					return null;
				}
				Note n = new Note();
				n.setUe(ue);
				try {
					n.setValeur(new Double(res[3]));
				} catch (NumberFormatException e) {
					buff.close();
					return null;
				}
				n.setEtudiant(etu);
				ue.getNotes().add(n);
				ue.getEtudiants().add(etu);
			}
		} catch (IOException e) {
			return null;
		}
		
		try {
			buff.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ue;
	}
	
}
