package FL1_project.FL1_project.service.implantation;

import java.util.Collection;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.domain.UE;
import FL1_project.FL1_project.domain.persistence.AdministrateurDao;
import FL1_project.FL1_project.domain.persistence.EtudiantDao;
import FL1_project.FL1_project.domain.persistence.ProfesseurDao;
import FL1_project.FL1_project.domain.persistence.UEDao;
import FL1_project.FL1_project.service.specifications.IAdministrateurService;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC MichaÃ«l
 * @author LEMAIRE Alexandre Cette class est rÃ©servÃ©e aux administrateurs.
 *         Elle permet de crÃ©er des professeurs, des Ã©tudiants et des notes.
 */
public class AdministrateurService implements IAdministrateurService {
	ProfesseurDao professeurDao = new ProfesseurDao();
	EtudiantDao etudiantDao = new EtudiantDao();
	UEDao ueDao = new UEDao();
	
	/**
	 * La mÃ©thode supprimerUE permet de supprimer comme son nom l'indique une
	 * UE.</br> Cette mÃ©thode a besoin d'informations concernant l'Ue a
	 * supprimer. </br> Seule l'atribut nom de l'Ue est obligatoire pour la
	 * suppression. </br>
	 * 
	 * @param UE
	 *            Le nom de l'UE ne doit pas Ãªtre vide, ne doit pas Ãªtre
	 *            null</br>
	 * @return La mÃ©thode supprime l'ue passÃ©e comme paramettre. si il est pas
	 *         valide la fonction envoie une exception. </br>
	 * @throws Exception
	 */
	public void supprimerUE(String ue) throws Exception {
		if (ue == null)
			throw new Exception("Des champs sont vide ou null.");
		ueDao.remove(ue);
	}
	
	/**
	 * La mÃ©thode supprimerProfesseur permet de supprimer comme son nom
	 * l'indique un professeur.</br> Cette mÃ©thode a besoin d'informations
	 * concernant le professeur a supprimer. </br> Seule l'attribut identifiant
	 * du Professeur est obligatoire pour la suppression. </br>
	 * 
	 * @param Professeur
	 *            L'identifiant du professeur ne doit pas Ãªtre vide, ni
	 *            infÃ©rieur Ã  0</br>
	 * @return La mÃ©thode supprime le professeur passÃ© comme paramÃ¨tre, en
	 *         cas d'Ã©chec elle renvoie une Exception. </br>
	 * @throws Exception
	 */
	
	public void supprimerProfesseur(Integer prof) throws Exception {
		if (prof < 0)
			throw new Exception("Identifiant < 0");
		professeurDao.remove(prof);
	}
	
	/**
	 * La mÃ©thode supprimerEtudiant permet de supprimer comme son nom l'indique
	 * un etudiant.</br> Cette mÃ©thode a besoin d'informations concernant
	 * l'Ã©tudiant a supprimer. </br> Seule l'attribut NumEtudiant de l'etudiant
	 * est obligatoire pour la suppression. </br>
	 * 
	 * @param Etudiant
	 *            Le NumEtudiant de l'Ã©tudiant ne doit pas Ãªtre vide, ni
	 *            infÃ©rieur Ã  0</br>
	 * @return La mÃ©thode supprime l'Ã©tudiant passÃ© comme paramettre, en cas
	 *         d'echec elle renvoie une Exception. </br>
	 * @throws Exception
	 */
	public void supprimerEtudiant(Integer etu) throws Exception {
		if (etu < 0)
			throw new Exception("numÃ©ro < 0");
		etudiantDao.remove(etu);
	}
	
	/**
	 * La mÃ©thode creerProfesseur permet de crÃ©er comme son nom l'indique un
	 * professeur et l'inserer dans la base de donnÃ©e.</br> Pour cette
	 * crÃ©ation la mÃ©thode a besoin d'informations concernant le professeur
	 * qui sont passÃ©s en argument.</br> Un professeur est caractÃ©risÃ© par
	 * son nom, son prÃ©nom et son numÃ©ro d'identifiant qui est unique.
	 * 
	 * @param nom
	 *            Le nom ne doit pas Ãªtre vide, ne doit pas Ãªtre null, ne doit
	 *            contenir que des lettres, des espaces et des tirets.</br>
	 * @param prenom
	 *            Le prÃ©nom ne doit pas Ãªtre vide, ne doit pas Ãªtre null, ne
	 *            doit contenir que des lettres, des espaces et des tirets.</br>
	 * @return La mÃ©thode renvoit une instance du domaine de la class
	 *         Professeur si toutes les spÃ©cifications ci-dessus sont corrects,
	 *         sinon elle Ã©choue et renvoit une exception.</br>
	 * @throws Exception
	 */
	public Professeur creerProfesseur(String nom, String prenom)
			throws Exception {
		if (nom == "" || nom == null || prenom == null || prenom == "")
			throw new Exception("Des champs sont vide ou null.");
		if (!nom.matches("[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]*")
				|| !prenom
						.matches("[a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]*"))
			throw new Exception(
					"Les champs nom et/ou prÃ©nom comportent des caractÃ¨res incorrects");
		
		Professeur professeur = new Professeur();
		professeur.setNom(nom);
		professeur.setPrenom(prenom);
		professeurDao.insert(professeur);
		return professeur;
	}
	
	/**
	 * La mÃ©thode creerEtudiant permet de crÃ©er comme son nom l'indique un
	 * Ã©tudiant et l'inserer dans la base de donnÃ©e..</br> Pour cette
	 * crÃ©ation la mÃ©thode a besoin d'informations concernant l'Ã©tudiant qui
	 * sont passÃ©s en argument.</br> Un Ã©tudiant est caractÃ©risÃ© par son
	 * nom, son prÃ©nom et son numÃ©ro d'Ã©tudiant qui est unique.
	 * 
	 * @param nom
	 *            Le nom ne doit pas Ãªtre vide, ne doit pas Ãªtre null, ne doit
	 *            contenir que des lettres, des espaces et des tirets.</br>
	 * @param prenom
	 *            Le prÃ©nom ne doit pas Ãªtre vide, ne doit pas Ãªtre null, ne
	 *            doit contenir que des lettres, des espaces et des tirets.</br>
	 * @return La mÃ©thode renvoit une instance du domaine de la class Etudiant
	 *         si toutes les spÃ©cifications ci-dessus sont corrects, sinon elle
	 *         Ã©choue et renvoit une exception.</br>
	 * @throws Exception
	 */
	public Etudiant creerEtudiant(String nom, String prenom) throws Exception {
		if (nom == "" || nom == null || prenom == null || prenom == "")
			throw new Exception("Des champs sont vide ou null.");
		
		if (!nom.matches("[a-zA-Z- àâäçéèêëîïôöùûüÀÂÄÇÉÈÊËÎÏÔÖÙÛÜÆŒæœ]*")
				|| !prenom
						.matches("[a-zA-Z- àâäçéèêëîïôöùûüÀÂÄÇÉÈÊËÎÏÔÖÙÛÜÆŒæœ]*"))
			throw new Exception(
					"Les champs nom et/ou prénom comportent des caractères incorrects");
		
		Etudiant etudiant = new Etudiant();
		etudiant.setNom(nom);
		etudiant.setPrenom(prenom);
		etudiantDao.insert(etudiant);
		return etudiant;
	}
	
	/**
	 * La mÃ©thode creerEU permet de crÃ©er comme son nom l'indique uen UE et
	 * l'inserer dans la base de donnÃ©es.</br> Une UE est composÃ©e simplement
	 * de son nom</br>
	 * 
	 * @param nom
	 *            Le nom ne doit pas Ãªtre vide, ne doit pas Ãªtre null.</br>
	 * @return La mÃ©thode renvoit une instance du domaine de la class UE si
	 *         toutes les spÃ©cifications ci-dessus sont correctes, sinon elle
	 *         Ã©choue et renvoit null.</br>
	 * @throws Exception
	 */
	public UE creerUE(String nom, Integer integer) throws Exception {
		if (nom == "" || nom == null)
			throw new Exception("Le champ nom est vide ou null");
		
		UE ue = new UE();
		ue.setNom(nom);
		ueDao.insert(ue, integer);
		return ue;
	}
	
	public Collection<UE> getUes() {
		return ueDao.getAllUes();
	}
	
	public boolean estCorrectPasswd(int idf, String passwd, String role)
			throws Exception {
		if (passwd == null) {
			return false;
		}
		if (role.equals("etudiant")) {
			EtudiantService etu = new EtudiantService();
			Integer numEtu = idf;
			
			Etudiant etudiant = etu.etudiantById(numEtu);
			int Nbpasswd = 0;
			for (int i = 0; i < etudiant.getNom().length(); ++i) {
				Nbpasswd += etudiant.getNom().charAt(i);
			}
			System.out.println("Password de l'utilisateur : " + Nbpasswd);
			if (!passwd.equals(new Integer(Nbpasswd).toString()))
				return false;
		} else if (role.equals("professeur")) {
			
			ProfesseurService prof = new ProfesseurService();
			Integer numProf = idf;
			Professeur professeur = prof.professeurById(numProf);
			int Nbpasswd = 0;
			for (int i = 0; i < professeur.getNom().length(); ++i) {
				Nbpasswd += professeur.getNom().charAt(i);
			}
			System.out.println("Password de l'utilisateur : " + Nbpasswd);
			
			if (!passwd.equals(new Integer(Nbpasswd).toString()))
				return false;
		} else if (role.equals("administrateur")) {
			System.out.println("passe Administrateur");
			AdministrateurDao admD = new AdministrateurDao();
			if (!admD.existAdm(passwd))
				return false;
		}
		return true;
	}
	/*
	 * TODO: ce service (ou le service Professeur) doit pouvoir attribuer un
	 * professeur Ã  une UE, cette fonction est manquante
	 */
}
