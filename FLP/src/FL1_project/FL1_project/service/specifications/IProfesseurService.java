package FL1_project.FL1_project.service.specifications;

import java.util.Collection;

import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.domain.Professeur;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public interface IProfesseurService {
	public Note modifierNoteEtudiant(Note note, double Note) throws Exception;
	
	public Collection<Note> consulterToutesNotes(String UE) throws Exception;
	
	public Collection<Professeur> getAllProfesseur() throws Exception;
	
}
