package FL1_project.FL1_project.service.specifications;

import FL1_project.FL1_project.domain.UE;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public interface IUECsvService {
	public boolean construireFichier(UE ue);
	
	public UE lireFichier(String fileName);
}
