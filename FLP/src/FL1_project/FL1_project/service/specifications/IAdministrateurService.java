package FL1_project.FL1_project.service.specifications;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.domain.UE;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public interface IAdministrateurService {
	public Professeur creerProfesseur(String nom, String prenom)
			throws Exception;
	
	public Etudiant creerEtudiant(String nom, String prenom) throws Exception;
	
	public UE creerUE(String nom, Integer i) throws Exception;
	
	public void supprimerProfesseur(Integer prof3) throws Exception;
	
	public void supprimerEtudiant(Integer etu4) throws Exception;
	
	public void supprimerUE(String ue2) throws Exception;
}
