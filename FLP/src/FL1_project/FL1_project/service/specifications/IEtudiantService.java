package FL1_project.FL1_project.service.specifications;

import java.util.Collection;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Note;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public interface IEtudiantService {
	public Note consulterNote(String ue) throws Exception;
	
	public void setEtudiant(Etudiant etud) throws Exception;
	
	public Collection<Etudiant> getAllEtudiants() throws Exception;
}
