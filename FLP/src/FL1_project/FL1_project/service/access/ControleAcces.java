package FL1_project.FL1_project.service.access;

import FL1_project.FL1_project.service.implantation.AdministrateurService;

public class ControleAcces {
	private Session session;
	private int nbACCESMax;
	private int nbTentatives;
	private int utilisateurPrecedent;
	private AdministrateurService admin;
	
	public ControleAcces(AdministrateurService a) {
		this.nbTentatives = 0;
		this.nbACCESMax = 3;
		this.admin = a;
	}
	
	/**
	 * Donne une session en cas de reussite
	 * 
	 * @param idf
	 * @param passwd
	 * @throws NbMaxTentativesDepasseException
	 * @throws UtilisateurInconnuException
	 */
	public void accesSession(int idf, String passwd, String role)
			throws NbMaxTentativesDepasseException, UtilisateurInconnuException {
		this.utilisateurPrecedent = idf;
		try {
			if (!this.admin.estCorrectPasswd(idf, passwd, role)) {
				gereEchecAcces();
			} else {
				donneAcces(idf, passwd, role);
			}
		} catch (Exception e) {
			e.printStackTrace();
			gereEchecAcces();
		}
	}
	
	/**
	 * Genere les exceptions en cas d'echec de connection
	 * 
	 * @throws NbMaxTentativesDepasseException
	 * @throws UtilisateurInconnuException
	 */
	
	private void gereEchecAcces() throws NbMaxTentativesDepasseException,
			UtilisateurInconnuException {
		++nbTentatives;
		if (nbTentatives >= nbACCESMax)
			throw new NbMaxTentativesDepasseException();
		throw new UtilisateurInconnuException();
	}
	
	/**
	 * Créer une session
	 * 
	 * @param idf
	 * @param passwd
	 * @return
	 */
	
	private Session donneAcces(int idf, String passwd, String role) {
		System.out.println("passe ici");
		this.nbTentatives = 0;
		return (this.session = new Session(idf, passwd, role));
	}
	
	public Session getSession() {
		return session;
	}
	
	public void setSession(Session session) {
		this.session = session;
	}
	
	public int getNbACCESMax() {
		return nbACCESMax;
	}
	
	public void setNbACCESMax(int nbACCESMax) {
		this.nbACCESMax = nbACCESMax;
	}
	
	public int getNbTentatives() {
		return nbTentatives;
	}
	
	public void setNbTentatives(int nbTentatives) {
		this.nbTentatives = nbTentatives;
	}
	
	public int getUtilisateurPrecedent() {
		return utilisateurPrecedent;
	}
	
	public void setUtilisateurPrecedent(int utilisateurPrecedent) {
		this.utilisateurPrecedent = utilisateurPrecedent;
	}
	
	public AdministrateurService getAdmin() {
		return admin;
	}
	
	public void setAdmin(AdministrateurService admin) {
		this.admin = admin;
	}
	
}
