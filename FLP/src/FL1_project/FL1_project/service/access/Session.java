package FL1_project.FL1_project.service.access;

public class Session {
	private int idf;
	private String passwd;
	private String role;
	
	public Session(int idf, String passwd, String role) {
		this.idf = idf;
		this.passwd = passwd;
		this.role = role;
	}
	
	public int getIdf() {
		return idf;
	}
	
	public String getPasswd() {
		return passwd;
	}
	
	public String getRole() {
		return role;
	}
	
}
