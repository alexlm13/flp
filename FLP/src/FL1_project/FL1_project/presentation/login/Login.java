package FL1_project.FL1_project.presentation.login;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import FL1_project.FL1_project.service.access.ControleAcces;
import FL1_project.FL1_project.service.access.NbMaxTentativesDepasseException;
import FL1_project.FL1_project.service.access.Session;
import FL1_project.FL1_project.service.access.UtilisateurInconnuException;
import FL1_project.FL1_project.service.implantation.AdministrateurService;

/**
 * The Class Login.
 *
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette classe gere l'authentification d'un utilisateur
 */
public class Login extends HttpServlet {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The controlaccess. */
	ControleAcces controlaccess = new ControleAcces(new AdministrateurService());
	
	public void init(ServletConfig c) throws ServletException {
	}
	
	public void destroy() {
	}
	
	/**
	 * Do get, si mode logout se deconnecte.
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("mode").equals("logOut")) {
			System.out.println("passe");
			request.getSession().setAttribute("session", null);
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
			return;
		}
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}
	
	/**
	 * Do post, gere l'envoi du formulaire de connexion, si ok authentifie
	 * l'utilisateur, sinon le bloque
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String role = request.getParameter("role");
		String login = request.getParameter("login");
		String pass = request.getParameter("pass");
		
		if (role == null) {
			request.getSession().setAttribute("error",
					"Erreur de connection : login/pass/role null.");
			request.getRequestDispatcher("login.jsp")
					.forward(request, response);
			return;
		}
		
		int idf = -1;
		try {
			if (!role.equals("administrateur")) {
				idf = new Integer(login);
			}
		} catch (NumberFormatException e) {
			try {
				controlaccess.accesSession(-1, null, role);
			} catch (NbMaxTentativesDepasseException e1) {
				request.getSession()
						.setAttribute(
								"error",
								"Erreur de connection : login/pass/role incorrect(s). Nombre de tentative dépassé.");
				request.getRequestDispatcher("login.jsp").forward(request,
						response);
				return;
			} catch (UtilisateurInconnuException e1) {
				request.getSession().setAttribute("error",
						"Erreur de connection : login/pass/role incorrect(s).");
				request.getRequestDispatcher("login.jsp").forward(request,
						response);
				return;
			} catch (Exception e1) {
				request.getSession().setAttribute("error",
						"Erreur de connection : login/pass/role incorrect(s).");
				request.getRequestDispatcher("login.jsp").forward(request,
						response);
				return;
			}
			return;
		}
		Session session = new Session(idf, pass, role);
		try {
			controlaccess.accesSession(idf, pass, role);
		} catch (NbMaxTentativesDepasseException e) {
			request.getSession()
					.setAttribute(
							"error",
							"Erreur de connection : login/pass/role incorrect(s). Nombre de tentative dépassé.");
			request.getRequestDispatcher("login.jsp")
					.forward(request, response);
			return;
		} catch (UtilisateurInconnuException e) {
			request.getSession().setAttribute("error",
					"Erreur de connection : login/pass/role incorrect(s).");
			request.getRequestDispatcher("login.jsp")
					.forward(request, response);
			return;
		} catch (Exception e) {
			request.getSession().setAttribute("error",
					"Erreur de connection : login/pass/role incorrect(s).");
			request.getRequestDispatcher("login.jsp")
					.forward(request, response);
			return;
		}
		controlaccess = new ControleAcces(new AdministrateurService());
		request.getSession().setAttribute("session", session);
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
}