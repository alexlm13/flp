package FL1_project.FL1_project.presentation.professeur;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.service.access.Session;
import FL1_project.FL1_project.service.implantation.AdministrateurService;

/**
 * The Class EditProfesseur.
 *
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette classe ajout/supprime un professeur
 */
@WebServlet("/EditProfesseur")
public class EditProfesseur extends HttpServlet {
	AdministrateurService adminService;
	private static final long serialVersionUID = 1L;
	
	public void init(ServletConfig c) throws ServletException {
		adminService = new AdministrateurService();
		
	}
	
	public void destroy() {
	}
	
	/**
	 * Do get, si role = administration, on supprime le professeur par son
	 * identifiant, sinon bloque
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Session session = (Session) request.getSession()
				.getAttribute("session");
		
		if (HasRole("administrateur", session)) {
			if (request.getParameter("mode") != null
					&& request.getParameter("mode").equals("delete")) {
				int prof = new Integer(request.getParameter("id"));
				try {
					adminService.supprimerProfesseur(prof);
					request.getSession().setAttribute("info",
							"Le professeur " + prof + " a bien été supprimé.");
					request.getRequestDispatcher("index.jsp").forward(request,
							response);
				} catch (Exception e) {
					e.printStackTrace();
					request.getSession().setAttribute("error",
							"vous ne pouvez pas supprimer ce prof!!");
					request.getRequestDispatcher("index.jsp").forward(request,
							response);
				}
				
			} else {
				request.getRequestDispatcher("creeProfesseur.jsp").forward(
						request, response);
			}
		} else {
			request.getSession().setAttribute("error",
					new Error("user unknown"));
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}
	}
	
	/**
	 * Do post, si role = administration, on ajoute le professeur, sinon bloque
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Professeur professeur = new Professeur();
		int nb = 0;
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		professeur.setNom(nom);
		professeur.setPrenom(prenom);
		Professeur profError = validate(professeur);
		if (profError == null) {
			try {
				adminService.creerProfesseur(nom, prenom);
				for (int i = 0; i < nom.length(); ++i) {
					nb += nom.charAt(i);
				}
				request.getSession()
						.setAttribute(
								"info",
								"Le professeur "
										+ nom
										+ " "
										+ prenom
										+ " a bien été créé. Son ID est disponible dans la liste des professeurs. Son password est le "
										+ nb);
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			} catch (Exception e) {
				request.getSession().setAttribute("error",
						"Vous ne  pouvez pas completez cette action!");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
		} else {
			request.getSession().setAttribute("professeur", professeur);
			request.getSession().setAttribute("professeurError", profError);
			
			request.getRequestDispatcher("creeProfesseur.jsp").forward(request,
					response);
		}
		
	}
	
	@Deprecated
	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) {
		int numProf = new Integer(request.getParameter("numProfesseur"));
		try {
			adminService.supprimerProfesseur(numProf);
			request.getSession().setAttribute("info",
					"Le professeur " + numProf + " a bien été supprimé");
		} catch (Exception e) {
			request.getSession().setAttribute("error",
					"vous pouvez pas suprimer ce professeur");
		}
	}
	
	/**
	 * Teste le role d'une personne au sein de l'application Si role =
	 * professeur, modifie la note envoy� en argument, sinon bloqu�
	 *
	 * @param string
	 *            role
	 * @param session
	 *            session courrante
	 */
	private boolean HasRole(String string, Session session) {
		return session != null && session.getRole().equals(string);
	}
	
	/**
	 * Teste si le professeur pass� en argument est valide
	 *
	 * @param professeur
	 *            professeur courant
	 */
	private Professeur validate(Professeur professeur) {
		if (professeur.getNom() == "" || professeur.getNom() == null) {
			professeur.setNom("Le nom ne doit pas etre null ou vide");
			return professeur;
			
		}
		if (professeur.getPrenom() == "" || professeur.getPrenom() == null) {
			professeur.setNom("Le prenom ne doit pas etre null ou vide");
			return professeur;
			
		}
		
		return null;
	}
	
	public void service() {
	}
	
}