package FL1_project.FL1_project.presentation.professeur;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.service.access.Session;
import FL1_project.FL1_project.service.implantation.ProfesseurService;

/**
 * The Class EditProfesseur.
 *
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette classe liste les professeurs
 */
public class ListProfesseur extends HttpServlet {
	ProfesseurService professeurService;
	private static final long serialVersionUID = 1L;
	
	public void init(ServletConfig c) throws ServletException {
		professeurService = new ProfesseurService();
	}
	
	public void destroy() {
	}
	
	/**
	 * Do get, on recupere tous les professeurs
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Collection<Professeur> professeurs = new HashSet<Professeur>();
		try {
			professeurs = professeurService.getAllProfesseur();
			request.getSession().setAttribute("professeurs", professeurs);
			request.getRequestDispatcher("listProf.jsp").forward(request,
					response);
		} catch (Exception e) {
			request.getSession().setAttribute("error",
					"vous ne pouvez pas consulter la liste des professeurs!!");
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}
	}
	
	/**
	 * Teste le role d'une personne au sein de l'application Si role =
	 * professeur, modifie la note envoy� en argument, sinon bloqu�
	 *
	 * @param string
	 *            role
	 * @param session
	 *            session courrante
	 */
	private boolean HasRole(String string, Session connection) {
		return connection != null && connection.getRole().equals(string);
	}
	
	public void service() {
	}
	
}