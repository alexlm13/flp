package FL1_project.FL1_project.presentation.student;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.service.access.Session;
import FL1_project.FL1_project.service.implantation.EtudiantService;
import FL1_project.FL1_project.service.implantation.ProfesseurService;

/**
 * Cette classe s'occupe de lister les etudiants. Utilisateur : Administrateur,
 * professeur.
 */
public class ListEtudiant extends HttpServlet {
	EtudiantService etudiantService;
	ProfesseurService professeurService;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// initialisation de la servlet
	public void init(ServletConfig c) throws ServletException {
		etudiantService = new EtudiantService();
		professeurService = new ProfesseurService();
	}
	
	// destruction la servlet
	public void destroy() {
	}
	
	/**
	 * Lister tous les etudiants, les etudiants d'un professeur, ou d'une UE,
	 * celon les parametres passes par l'utilisateur.
	 * 
	 * @throws Lève
	 *             une exception si une erreur s'est produite ou une erreur
	 *             d'authentification si l'utilisateur n'est pas authentifié.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Collection<Etudiant> etudiants = new HashSet<Etudiant>();
		try {
			Session session = (Session) request.getSession().getAttribute(
					"session");
			
			System.out.println(request.getParameter("ue"));
			System.out.println(session.getIdf());
			
			if (HasRole("professeur", session)) {
				Professeur professeur = professeurService
						.professeurById(new Integer(session.getIdf()));
				professeurService.setProfesseur(professeur);
				String ue = request.getParameter("ue");
				if (ue != null)
					etudiants = etudiantService.etudiantsByUe(ue);
				else
					etudiants = professeurService
							.etudiantsByProfesseur(new Integer(session.getIdf())
									.toString());
				
				System.out.println(etudiants);
				
				request.getSession().setAttribute("etudiants", etudiants);
				request.getRequestDispatcher("listEtudiant.jsp").forward(
						request, response);
				
			} else {
				etudiants = etudiantService.getAllEtudiants();
				request.getSession().setAttribute("etudiants", etudiants);
				request.getRequestDispatcher("listEtudiant.jsp").forward(
						request, response);
			}
		} catch (Exception e) {
			request.getSession().setAttribute("error",
					"vous ne pouvez pas consulter la liste des etudiants!!");
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}
		
		System.out.println(etudiants);
		
	}
	
	/**
	 * Verifier le role de l'utilisateur connecte.
	 */
	private boolean HasRole(String string, Session session) {
		return session != null && session.getRole().equals(string);
	}
	
	public void service() {
	}
	
}