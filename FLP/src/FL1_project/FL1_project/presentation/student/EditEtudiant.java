package FL1_project.FL1_project.presentation.student;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.service.access.Session;
import FL1_project.FL1_project.service.implantation.AdministrateurService;

/**
 * Cette classe s'occupe de la creation et supression d'une Etudiant.
 * Utilisateur : Administrateur.
 */
public class EditEtudiant extends HttpServlet {
	AdministrateurService adminService;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// initialisation de la servlet
	public void init(ServletConfig c) throws ServletException {
		adminService = new AdministrateurService();
		
	}
	
	// destruction la servlet
	public void destroy() {
	}
	
	/**
	 * Generation de formulaire de creation d'un etudiant
	 * 
	 * @throws Lève
	 *             une exception si une erreur s'est produite ou une erreur
	 *             d'authentification si l'utilisateur n'est pas authentifié.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Session session = (Session) request.getSession()
				.getAttribute("session");
		
		if (HasRole("administrateur", session)) {
			if (request.getParameter("mode") != null
					&& request.getParameter("mode").equals("delete")) {
				int etu = new Integer(request.getParameter("id"));
				try {
					adminService.supprimerEtudiant(etu);
					request.getSession().setAttribute("info",
							"L'étudiant " + etu + " a bien été supprimé.");
					request.getRequestDispatcher("index.jsp").forward(request,
							response);
				} catch (Exception e) {
					e.printStackTrace();
					request.getSession().setAttribute("error",
							"vous ne pouvez pas supprimer ce etudiant!!");
					request.getRequestDispatcher("index.jsp").forward(request,
							response);
				}
				
			} else {
				request.getRequestDispatcher("creeEtudiant.jsp").forward(
						request, response);
			}
		} else {
			request.getSession().setAttribute("error", "user unknown");
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}
		
	}
	
	/**
	 * Creation d'un étudiant dont l'instance est recupere par un formulaire
	 * remplit par un administrateur une fois la creation est fait correctement,
	 * l'utilisation sera redireger vers la page index.jsp avec un message de
	 * validation. le nom et le prenom de l'etudiant ne doivent pas etre null.
	 * 
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Etudiant etudiant = new Etudiant();
		int nb = 0;
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		System.out.println(nom + "passe " + prenom);
		
		etudiant.setNom(nom);
		etudiant.setPrenom(prenom);
		Etudiant etudiantError = validate(etudiant);
		if (etudiantError == null) {
			try {
				System.out.println("passeeeee");
				adminService.creerEtudiant(nom, prenom);
				for (int i = 0; i < nom.length(); ++i) {
					nb += nom.charAt(i);
				}
				request.getSession()
						.setAttribute(
								"info",
								"L'étudiant "
										+ nom
										+ " "
										+ prenom
										+ " a bien été créé. Son ID est disponible dans la liste des étudiants. Son mot de pass est "
										+ nb);
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			} catch (Exception e) {
				e.printStackTrace();
				request.getSession().setAttribute("error",
						"Vous ne  pouvez pas completez cette action!");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
			
		} else {
			request.getSession().setAttribute("etudiant", etudiant);
			request.getSession().setAttribute("etudiantError", etudiantError);
			request.getSession().setAttribute("error",
					"L'étudiant n'a pas été créé.");
			request.getRequestDispatcher("creeEtudiant.jsp").forward(request,
					response);
		}
		
	}
	
	/**
	 * Supression d'un etudiant dont son numEtudiant est passe comme parametre.
	 * 
	 * @throws Lève
	 *             une exception si une erreur s'est produite ou une erreur
	 *             d'authentification si l'utilisateur n'est pas authentifié.
	 */
	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) {
		int numEtudiant = new Integer(request.getParameter("numEtudiant"));
		try {
			adminService.supprimerEtudiant(numEtudiant);
		} catch (Exception e) {
			request.getSession().setAttribute("error",
					"vous ne pouvez pas suprimer cet etudiant");
		}
	}
	
	/**
	 * Verifier le role de l'utilisateur connecte..
	 */
	private boolean HasRole(String string, Session session) {
		return session != null && session.getRole().equals(string);
	}
	
	/**
	 * Validation de l'objets etudiant recupere par le formulaire.
	 */
	private Etudiant validate(Etudiant etudiant) {
		if (etudiant.getNom() == "" || etudiant.getNom() == null) {
			etudiant.setNom("Le nom ne doit pas etre null ou vide");
			return etudiant;
			
		}
		if (etudiant.getPrenom() == "" || etudiant.getPrenom() == null) {
			etudiant.setPrenom("Le prenom ne doit pas etre null ou vide");
			return etudiant;
			
		}
		
		return null;
	}
	
	public void service() {
	}
	
}