package FL1_project.FL1_project.presentation.note;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.service.access.Session;
import FL1_project.FL1_project.service.implantation.EtudiantService;

/**
 * The Class NoteEtudiant.
 *
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette classe recupere les notes d'un etudiant
 */
@WebServlet("/NoteEtudiant")
public class NoteEtudiant extends HttpServlet {
	EtudiantService etudiantService;
	private static final long serialVersionUID = 1L;
	
	public void init(ServletConfig c) throws ServletException {
		etudiantService = new EtudiantService();
	}
	
	public void destroy() {
	}
	
	/**
	 * Do get, si role = etudiant, on recupere la liste des notes de cette
	 * eleve, sinon bloque
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Session connection = (Session) request.getSession().getAttribute(
				"session");
		Etudiant etudiant = null;
		Note note = null;
		
		if (HasRole("etudiant", connection)) {
			try {
				etudiant = etudiantService.etudiantById(new Integer(connection
						.getIdf()));
				etudiantService.setEtudiant(etudiant);
				String ue = request.getParameter("ue");
				note = etudiantService.consulterNote(ue);
				request.getSession().setAttribute("note", note);
				request.getRequestDispatcher("note.jsp").forward(request,
						response);
			} catch (Exception e) {
				request.getSession().setAttribute("error",
						"vous ne pouvez pas completer cette action");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
		} else {
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
			request.getSession().setAttribute("error", "user unknown");
		}
		
		System.out.println(note);
		
	}
	
	/**
	 * Teste le role d'une personne au sein de l'application Si role =
	 * professeur, modifie la note envoy� en argument, sinon bloqu�
	 *
	 * @param string
	 *            role
	 * @param session
	 *            session courrante
	 */
	private boolean HasRole(String string, Session connection) {
		return connection != null && connection.getRole().equals(string);
	}
	
	public void service() {
	}
	
}