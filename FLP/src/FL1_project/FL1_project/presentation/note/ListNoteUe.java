package FL1_project.FL1_project.presentation.note;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.service.access.Session;
import FL1_project.FL1_project.service.implantation.ProfesseurService;

/**
 * The Class ListNoteUe.
 *
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette classe liste les notes
 */
public class ListNoteUe extends HttpServlet {
	ProfesseurService professeurService;
	private static final long serialVersionUID = 1L;
	
	public void init(ServletConfig c) throws ServletException {
		professeurService = new ProfesseurService();
	}
	
	public void destroy() {
	}
	
	/**
	 * Do get, si role = professeur, on recupere la liste des notes des eleves
	 * d'une Ue, sinon bloque
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Collection<Note> notes = new HashSet<Note>();
		Session session = (Session) request.getSession()
				.getAttribute("session");
		
		if (HasRole("professeur", session)) {
			try {
				Professeur professeur = professeurService
						.professeurById(new Integer(session.getIdf()));
				professeurService.setProfesseur(professeur);
				String ue = request.getParameter("ue");
				notes = professeurService.consulterToutesNotes(ue);
				request.getSession().setAttribute("notes", notes);
				request.getRequestDispatcher("note.jsp").forward(request,
						response);
			} catch (Exception e) {
				e.printStackTrace();
				request.getSession()
						.setAttribute("error",
								"Vous ne pouvez pas consulter la liste des note de cette ue!!");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
		} else {
			request.getSession().setAttribute("error", "Utilisateur inconnu!!");
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}
		
		System.out.println(notes);
		
	}
	
	/**
	 * Teste le role d'une personne au sein de l'application Si role =
	 * professeur, modifie la note envoy� en argument, sinon bloqu�
	 *
	 * @param string
	 *            role
	 * @param session
	 *            session courrante
	 */
	private boolean HasRole(String string, Session session) {
		return session != null && session.getRole().equals(string);
	}
	
	public void service() {
	}
	
}