/*
 * 
 */
package FL1_project.FL1_project.presentation.note;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.service.access.Session;
import FL1_project.FL1_project.service.implantation.EtudiantService;
import FL1_project.FL1_project.service.implantation.ProfesseurService;

/**
 * The Class EditNote.
 *
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette classe effectuer des ajouts/suppression de notes
 */
@WebServlet("/EditNote")
public class EditNote extends HttpServlet {
	ProfesseurService professeurService;
	EtudiantService etudiantService;
	
	private static final long serialVersionUID = 1L;
	
	public void init(ServletConfig c) throws ServletException {
		professeurService = new ProfesseurService();
		etudiantService = new EtudiantService();
	}
	
	public void destroy() {
	}
	
	/**
	 * Do post, si role = professeur, modifie la note envoy� en argument, sinon
	 * bloqu�
	 *
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             the servlet exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Session session = (Session) request.getSession()
				.getAttribute("session");
		
		if (HasRole("professeur", session)) {
			System.out.println(request.getParameter("etudiant"));
			System.out.println(request.getParameter("nomUe"));
			System.out.println(request.getParameter("note"));
			
			Integer idEtudiant = new Integer(request.getParameter("etudiant"));
			String nomUe = request.getParameter("nomUe");
			Double note = null;
			try {
				note = new Double(request.getParameter("note"));
			} catch (NumberFormatException e) {
				request.getSession().setAttribute("error",
						"Mauvais format de la note.");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
				return;
			}
			try {
				Etudiant etudiant = etudiantService.etudiantById(idEtudiant);
				etudiantService.setEtudiant(etudiant);
				Note noteAmodifier = etudiantService.consulterNote(nomUe);
				
				Professeur p = professeurService.professeurById(new Integer(
						session.getIdf()));
				professeurService.setProfesseur(p);
				professeurService.modifierNoteEtudiant(noteAmodifier, note);
				request.getSession().setAttribute("info",
						"La note a bien été changée.");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			} catch (Exception e) {
				request.getSession()
						.setAttribute(
								"error",
								"Une erreur s'est produite. Peut être vous n'avez pas les accès necessaire, ou une erreur de connection, ou la note n'est pas comprise entre 0 et 20.");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
		} else {
			request.getSession().setAttribute("error", "User unknown");
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}
		
	}
	
	/**
	 * Teste le role d'une personne au sein de l'application Si role =
	 * professeur, modifie la note envoy� en argument, sinon bloqu�
	 *
	 * @param string
	 *            role
	 * @param session
	 *            session courante
	 */
	private boolean HasRole(String string, Session session) {
		return session != null && session.getRole().equals(string);
	}
	
	public void service() {
	}
	
}