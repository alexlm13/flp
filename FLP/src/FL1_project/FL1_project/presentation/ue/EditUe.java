package FL1_project.FL1_project.presentation.ue;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.domain.UE;
import FL1_project.FL1_project.service.access.Session;
import FL1_project.FL1_project.service.implantation.AdministrateurService;
import FL1_project.FL1_project.service.implantation.ProfesseurService;

/**
 * Cette classe s'occupe de la creation et supression d'une ue. Utilisateur :
 * Administrateur.
 */
public class EditUe extends HttpServlet {
	AdministrateurService adminService;
	ProfesseurService professeurService;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// initialisation de la servlet
	public void init(ServletConfig c) throws ServletException {
		adminService = new AdministrateurService();
		professeurService = new ProfesseurService();
	}
	
	// destruction la servlet
	public void destroy() {
	}
	
	/**
	 * Generation de formulaire de creation d'une Ue, ou suprimer une UE si le
	 * parametre mode = delete.
	 * 
	 * @throws Lève
	 *             une exception si une erreur s'est produite ou une erreur
	 *             d'authentification si l'utilisateur n'est pas authentifié.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Session session = (Session) request.getSession()
				.getAttribute("session");
		
		if (HasRole("administrateur", session)) {
			if (request.getParameter("mode") != null
					&& request.getParameter("mode").equals("delete")) {
				String ue = (String) request.getParameter("ue");
				try {
					adminService.supprimerUE(ue);
					request.getSession().setAttribute("info",
							"L'ue " + ue + " a bien été supprimée.");
					request.getRequestDispatcher("index.jsp").forward(request,
							response);
				} catch (Exception e) {
					e.printStackTrace();
					request.getSession().setAttribute("error",
							"Vous ne pouvez pas supprimer cette Ue!!");
					request.getRequestDispatcher("index.jsp").forward(request,
							response);
				}
				
			} else {
				Collection<Professeur> professeurs = new HashSet<Professeur>();
				try {
					professeurs = professeurService.getAllProfesseur();
					request.getSession().setAttribute("professeurs",
							professeurs);
					request.getRequestDispatcher("creeUe.jsp").forward(request,
							response);
				} catch (Exception e) {
					e.printStackTrace();
					request.getSession()
							.setAttribute("error",
									"Vous ne pouvez pas consulter la liste des professeurs!!");
					request.getRequestDispatcher("index.jsp").forward(request,
							response);
				}
			}
		} else {
			request.getSession().setAttribute("error", "user unknown");
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}
		
	}
	
	/**
	 * Creation d'une UE dont l'instance est recupere par un formulaire remplit
	 * par un administrateur une fois la creation est fait correctement,
	 * l'utilisation sera rediriger vers la page index.jsp avec un message de
	 * validation. le nom et le prenom de l'etudiant ne doivent pas etre null.
	 * 
	 * @throws Lève
	 *             une exception si une erreur s'est produite ou une erreur
	 *             d'authentification si l'utilisateur n'est pas connecté.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UE ue = new UE();
		
		String nom = request.getParameter("nom");
		String prof = request.getParameter("prof");
		
		ue.setNom(nom);
		
		UE ueError = validate(ue);
		if (ueError == null) {
			try {
				adminService.creerUE(nom, new Integer(prof));
				request.getSession().setAttribute("info",
						"L'ue " + nom + " a bien été créée.");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			} catch (Exception e) {
				e.printStackTrace();
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
				request.getSession().setAttribute("error",
						"Vous ne pouvez pas completez cette action !");
			}
			
		} else {
			request.getSession().setAttribute("ue", ue);
			request.getSession().setAttribute("ueError", ueError);
			
			request.getRequestDispatcher("creeUe.jsp").forward(request,
					response);
		}
		
	}
	
	/**
	 * Supression d'une UE dont son nom est passe comme parametre.
	 * 
	 * @throws Lève
	 *             une exception si une erreur s'est produite ou une erreur
	 *             d'authentification si l'utilisateur n'est pas authentifié.
	 */
	public void doDelete(HttpServletRequest request,
			HttpServletResponse response) {
		String nomUe = request.getParameter("nomUe");
		try {
			adminService.supprimerUE(nomUe);
			request.getSession().setAttribute("info",
					"L'ue " + nomUe + " a bien été supprimée.");
		} catch (Exception e) {
			request.getSession().setAttribute("error",
					"Vous pouvez pas supprimer cette UE.");
		}
	}
	
	private boolean HasRole(String string, Session session) {
		return session != null && session.getRole().equals(string);
	}
	
	private UE validate(UE ue) {
		if (ue.getNom() == "" || ue.getNom() == null) {
			ue.setNom("Le nom ne doit pas etre null ou vide");
			return ue;
		}
		return null;
	}
	
	public void service() {
	}
	
}