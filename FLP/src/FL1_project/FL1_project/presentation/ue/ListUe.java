package FL1_project.FL1_project.presentation.ue;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.domain.UE;
import FL1_project.FL1_project.service.access.Session;
import FL1_project.FL1_project.service.implantation.AdministrateurService;
import FL1_project.FL1_project.service.implantation.EtudiantService;
import FL1_project.FL1_project.service.implantation.ProfesseurService;

/**
 * Cette classe s'occupe de lister les ues des differents utilisateurs de notre
 * application.
 */
public class ListUe extends HttpServlet {
	ProfesseurService professeurService;
	EtudiantService etudiantService;
	AdministrateurService admService;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// initialisation de la servlet
	public void init(ServletConfig c) throws ServletException {
		professeurService = new ProfesseurService();
		etudiantService = new EtudiantService();
		admService = new AdministrateurService();
	}
	
	// destruction la servlet
	public void destroy() {
	}
	
	/**
	 * Utilisateur connecter comme Professeur: lister toutes ses UES.
	 * Utilisateur connecter comme Etudiant: lister toutes ses UES et les UES ou
	 * il n'est pas inscrit.
	 * 
	 * @throws Lève
	 *             une exception si une erreur s'est produite ou une erreur
	 *             d'authentification si l'utilisateur n'est pas authentifié.
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Session session = (Session) request.getSession()
				.getAttribute("session");
		System.out.println("aaa :" + session);
		
		if (HasRole("professeur", session)) {
			Collection<UE> ues = new HashSet<UE>();
			Professeur professeur = null;
			System.out.println("bbb :" + session.getIdf());
			
			try {
				professeur = professeurService.professeurById(new Integer(
						session.getIdf()));
				professeurService.setProfesseur(professeur);
				ues = professeurService.getUesOfProfesseur();
				request.getSession().setAttribute("ues", ues);
				request.getRequestDispatcher("professeur.jsp").forward(request,
						response);
			} catch (Exception e) {
				e.printStackTrace();
				request.getSession().setAttribute("error",
						"vous ne pouvez pas consulter la liste des ues!!");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
			
			System.out.println(ues);
		} else if (HasRole("etudiant", session)) {
			System.out.println("etu");
			System.out.println(session.getIdf());
			try {
				Map<String, String> ueAvecNoteEtudiant = new HashMap<String, String>();
				Collection<UE> uesPasInscris = null;
				try {
					etudiantService.notesByEtudiant(new Integer(session
							.getIdf()));
					
					uesPasInscris = etudiantService
							.selectNotAssociateUEEtu(new Integer(session
									.getIdf()));
					ueAvecNoteEtudiant = etudiantService
							.notesByEtudiant(new Integer(session.getIdf()));
					request.getSession().setAttribute("ueAvecNoteEtudiant",
							ueAvecNoteEtudiant);
					request.getSession().setAttribute("uesPasInscris",
							uesPasInscris);
					request.getRequestDispatcher("etudiant.jsp").forward(
							request, response);
					
				} catch (Exception e) {
					e.printStackTrace();
					request.getSession().setAttribute("error",
							"vous ne pouvez pas consulter la liste des ues!!");
					request.getRequestDispatcher("index.jsp").forward(request,
							response);
				}
				
				System.out.println("ue : ok " + ueAvecNoteEtudiant.size());
				System.out.println("ue : pas ok " + uesPasInscris.size());
				
			} catch (NumberFormatException e) {
				request.getSession().setAttribute("error",
						"vous ne pouvez pas consulter la liste des ues!!");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
			
		} else if (HasRole("administrateur", session)) {
			try {
				Collection<UE> ues = new HashSet<UE>();
				ues = admService.getUes();
				request.getSession().setAttribute("ues", ues);
				request.getRequestDispatcher("listUe.jsp").forward(request,
						response);
				System.out.print(ues);
				System.out.print(admService);
			} catch (Exception e) {
				e.printStackTrace();
				request.getSession().setAttribute("error",
						"vous ne pouvez pas consulter la liste des ues!!");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
		} else {
			request.getSession().setAttribute("error", "user unknown");
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}
		
	}
	
	/**
	 * Etudiants connecte: s'inscrire a une UE dont son nom est passe comme
	 * parametre.
	 * 
	 * @throws Lève
	 *             une exception si une erreur s'est produite ou erreur
	 *             d'authentification si l'utilisateur n'est pas connecte comme
	 *             un etudiant.
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Session session = (Session) request.getSession()
				.getAttribute("session");
		
		String nomUe = request.getParameter("nomUe");
		if (HasRole("etudiant", session)) {
			try {
				etudiantService.addEtudiantUe(new Integer(session.getIdf()),
						nomUe);
				request.getSession().setAttribute("info",
						"Vous vous êtes bien inscrit à l'ue " + nomUe + " ");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			} catch (Exception e) {
				e.printStackTrace();
				request.getSession().setAttribute("error",
						"vous ne pouvez pas completez cet action!!");
				request.getSession().setAttribute("info",
						"Erreur lors de l'inscription à l'ue " + nomUe + " ");
				request.getRequestDispatcher("index.jsp").forward(request,
						response);
			}
		} else {
			request.getSession().setAttribute("error", "user unknown");
			request.getRequestDispatcher("index.jsp")
					.forward(request, response);
		}
	}
	
	private boolean HasRole(String string, Session session) {
		return session != null && session.getRole().equals(string);
	}
	
	public void service() {
	}
	
}