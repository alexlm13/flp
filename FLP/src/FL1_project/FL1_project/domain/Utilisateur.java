package FL1_project.FL1_project.domain;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public interface Utilisateur {
	public String droits();
}
