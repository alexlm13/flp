package FL1_project.FL1_project.domain;

import java.io.Serializable;

import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public class Administrateur implements Serializable {
	private @NonNull String motDePass;
	
	public Administrateur() {
		motDePass = "";
	}
	
	@EnsuresNonNull(value = { "motDePass" })
	public String getMotDePass() {
		return motDePass;
	}
	
	@RequiresNonNull(value = { "motDePass" })
	public void setMotDePass(String motDePass) {
		this.motDePass = motDePass;
	}
	
}
