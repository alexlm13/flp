package FL1_project.FL1_project.domain;

import java.io.Serializable;

import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public class Note implements Serializable {
	private @NonNull double valeur;
	private @NonNull UE ue;
	private @NonNull Etudiant etudiant;
	
	public Note() {
		valeur = -1.0;
		ue = new UE();
		etudiant = new Etudiant();
		
	}
	
	@EnsuresNonNull(value = { "etudiant" })
	public Etudiant getEtudiant() {
		return etudiant;
	}
	
	@RequiresNonNull(value = { "etudiant" })
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	
	@EnsuresNonNull(value = { "ue" })
	public UE getUe() {
		return ue;
	}
	
	@RequiresNonNull(value = { "ue" })
	public void setUe(UE ue) {
		this.ue = ue;
	}
	
	@EnsuresNonNull(value = { "valeur" })
	public double getValeur() {
		return valeur;
	}
	
	@RequiresNonNull(value = { "valeur" })
	public void setValeur(double valeur) {
		this.valeur = valeur;
	}
	
	@Override
	public String toString() {
		return "Note [valeur=" + valeur + "]";
	}
	
}
