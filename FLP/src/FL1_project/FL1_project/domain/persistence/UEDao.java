package FL1_project.FL1_project.domain.persistence;

import java.sql.SQLException;
import java.util.Collection;

import FL1_project.FL1_project.domain.UE;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre Cette classe permet de récupérer/supprimer/mettre à
 *         jour des données concernant les Ues.
 */
public class UEDao extends JdbcTools implements Dao<UE, String> {
	public String tableUE = "CREATE TABLE IF NOT EXISTS FL1_Ue("
			+ "nom Varchar(10) not null primary key," // attention, avec mysql,
			// null != '' (chaine
			// vide)
			+ "idProf Integer null,"
			+ "CONSTRAINT idProf FOREIGN KEY (idProf) REFERENCES FL1_Professeur(identifiant))";
	
	public String table8UES_Etudiants = "create table if not exists FL1_UesEtudiants( "
			+ "nomUE varchar(10) not null,"
			+ "numEtudiant Integer not null,"
			+ "CONSTRAINT nomueC FOREIGN KEY (nomUE) REFERENCES FL1_UE(nomUE),"
			+ "CONSTRAINT numetudiantC FOREIGN KEY (numEtudiant) REFERENCES FL1_Etudiant(numEtudiant))";
	
	public UEDao() {
		init();
	}
	
	/**
	 * Initialisation de la table de la base de donnée de l'Ue.
	 * 
	 * @throws SQLException
	 */
	public void initTable() throws SQLException {
		init();
		executeUpdate(tableUE);
		executeUpdate(table8UES_Etudiants);
	}
	
	/**
	 * Supression de l'Ue de la base de données dont l'instance est passé en
	 * paramètre.
	 * 
	 * @param UE
	 *            L'Ue à supprimer de la base de données, doit exister dans la
	 *            base de données.
	 * @throws Exception
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void remove(String t) throws Exception {
		try {
			executeUpdate("delete from FL1_Ue where nom = ?", t);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Insertion d'une Ue dans la base de données dont l'instance est passé en
	 * paramètre.
	 * 
	 * @param UE
	 *            L'ue à insérer dans la base de données. ne doit pas etre null
	 *            et ne doit pas exister dans la base de données. le nom de l'Ue
	 *            ne doit pas etre null.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void insert(UE t, Integer integer) throws Exception {
		System.out.println(integer);
		if (t == null)
			throw new Exception("UE null");
		if (t.getNom() == null)
			throw new Exception("nom null");
		try {
			if (t.getProfesseur() == null) {
				executeUpdate("insert into FL1_Ue (nom,idProf)  values (?,?)",
						t.getNom(), integer);
			} else {
				if (t.getProfesseur().getIdentifiant() < 0)
					throw new Exception("identifiant professeur < 0");
				executeUpdate("insert into FL1_Ue (nom,idProf)  values (?,?)",
						t.getNom(), integer);
			}
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Modification d'une Ue dans la base de données dont l'instance est passé
	 * en paramètre.
	 * 
	 * @param UE
	 *            L'ue à modifier dans la base de données, ne doit pas etre
	 *            null, doit exister dans la base de données.
	 * @param UE2
	 *            L'ue à modifier dans la base de données, ne doit pas etre
	 *            null.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void update(UE t, UE t2) throws Exception {
		if (t == null)
			throw new Exception("UE null");
		if (t2 == null)
			throw new Exception("UE null");
		if (t.getNom() == null)
			throw new Exception("nom null");
		if (t2.getProfesseur() == null)
			throw new Exception("professeur null");
		if (t2.getProfesseur().getIdentifiant() < 0)
			throw new Exception("identifiant professeur < 0");
		
		try {
			executeUpdate("update FL1_Ue set idProf= ? where nom = ?",
					t2.getProfesseur() == null ? null : t2.getProfesseur()
							.getIdentifiant(), t.getNom());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Récupération (dans la base de données) d'une ue via l'identifiant du
	 * professeur dont on souhaite récupérer sa liste d'ue.
	 * 
	 * @param identifiantProfesseur
	 *            l'identifiant du professeur dont on souhaite récupérer sa
	 *            liste d'ue doit etre supérieur à 0 et doit exister dans la
	 *            base de données.
	 * @return Collection<UE> La liste de toutes les ues contenues dans la base
	 *         de donnees dont l'identifiant du professeur est passé en
	 *         paramètre.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public Collection<UE> getUesOfProfesseur(Integer identifiantProfesseur) {
		Collection<UE> uesOfProfesseur = null;
		try {
			uesOfProfesseur = findBeans(
					"select nom, idProf from FL1_Ue where idProf=?",
					new ResultSetToUe(), identifiantProfesseur);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return uesOfProfesseur;
	}
	
	public void insertUeEtu(Integer etu, String ue) throws Exception {
		if (ue == null)
			throw new Exception("UE null");
		if (etu == null)
			throw new Exception("Etudiant null");
		
		try {
			executeUpdate(
					"insert into FL1_UesEtudiants (nomUE,numEtudiant)  values (?,?)",
					ue, etu);
			executeUpdate(
					"insert into FL1_Note (valeur, nomUE, numEtudiant)  values (?, ?, ?)",
					-1, ue, etu);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public Collection<UE> selectNotAssociateUEEtu(int numEtudiant)
			throws Exception {
		try {
			return findBeans(
					"Select * from FL1_Ue ue where ue.nom not in ( SELECT ueEtu.nomUe FROM  FL1_UesEtudiants ueEtu where ? = ueEtu.numEtudiant)",
					new ResultSetToUe(), numEtudiant);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		Collection<UE> Ues = new UEDao().selectNotAssociateUEEtu(2);
		
		for (UE ue : Ues) {
			System.out.println(ue.getNom());
		}
		System.out.println(Ues.size());
	}
	
	/**
	 * Récupération de toutes les ues existant dans la base de donnée
	 * 
	 * @return Collection<UE> La liste de toutes les ues contenues dans la base
	 *         de donnée
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public Collection<UE> getAllUes() {
		Collection<UE> ues = null;
		try {
			ues = findBeans("select nom,idProf from FL1_Ue ",
					new ResultSetToUe());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ues;
	}
	
	public UE getUeBy(String nom) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void insert(UE t) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
