package FL1_project.FL1_project.domain.persistence;

import java.sql.SQLException;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Interface de création de l'objet java.sql.ResultSet en objet T
 */
interface ResultSetToBean<T> {
	T toBean(java.sql.ResultSet rs) throws SQLException;
}