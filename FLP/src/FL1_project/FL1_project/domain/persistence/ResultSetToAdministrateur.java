package FL1_project.FL1_project.domain.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import FL1_project.FL1_project.domain.Administrateur;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre Cette class permet de creer via un ResultSet un
 *         Administrateur avec ses informations contenues dans le resultSet.
 */
class ResultSetToAdministrateur implements ResultSetToBean<Administrateur> {
	
	/**
	 * Cette méthode permet de creer une instance de la class Administrateur via
	 * les informations contenues dans java.sql.ResultSet
	 * 
	 * @param ResultSet
	 *            est le résultat d'une requette sur la base de donnée renvoyant
	 *            un/des résultats.
	 */
	@Override
	public Administrateur toBean(ResultSet rs) throws SQLException {
		Administrateur adm = new Administrateur();
		adm.setMotDePass(rs.getString(1));
		return adm;
	}
}