package FL1_project.FL1_project.domain.persistence;

import java.sql.SQLException;
import java.util.Collection;

import FL1_project.FL1_project.domain.Etudiant;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre Cette classe permet de récupérer/supprimer/mettre à
 *         jour des données concernant les étudiants.
 */
public class EtudiantDao extends JdbcTools implements Dao<Etudiant, Integer> {
	public static String tableEtudiant = "CREATE TABLE IF NOT EXISTS FL1_Etudiant ("
			+ "numEtudiant Integer primary key AUTO_INCREMENT,"
			+ "nom Varchar(50) not null," + "prenom Varchar(20) not null)";
	
	public EtudiantDao() {
		init();
	}
	
	/**
	 * Initialisation de la table de la base de donnée de l'Etudiant.
	 * 
	 * @throws SQLException
	 */
	public void initTable() throws SQLException {
		init();
		executeUpdate(tableEtudiant);
	}
	
	/**
	 * Supression de l'étudiant de la base de données dont l'instance est passé
	 * en paramètre.
	 * 
	 * @param Etudiant
	 *            L'étudiant à supprimer de la base de données. le numEtudiant
	 *            de l'etudiant ne doit pas etre null ni inferieur a 0 et doit
	 *            exister dans la base de données.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void remove(Integer numEtudiant) throws Exception {
		if (numEtudiant < 0)
			throw new Exception("Identifiant inférieur à 0");
		try {
			executeUpdate("delete from FL1_Etudiant where numEtudiant = ?",
					numEtudiant);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Insertion d'un étudiant dans la base de données dont l'instance est passé
	 * en paramètre.
	 * 
	 * @param Etudiant
	 *            L'étudiant à insérer dans la base de données. le nom et le
	 *            prenom de l'etudiant ne doivent pas etre null.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void insert(Etudiant t) throws Exception {
		if (t == null)
			throw new Exception("Etudiant null");
		else if (t.getNom() == null)
			throw new Exception("Nom null");
		else if (t.getPrenom() == null)
			throw new Exception("Prenom null");
		
		try {
			executeUpdate(
					"insert into FL1_Etudiant (nom, prenom)  values ( ?, ?)",
					t.getNom(), t.getPrenom());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Modification d'un étudiant dans la base de données dont l'instance est
	 * passé en paramètre.
	 * 
	 * @param Etudiant1
	 *            L'étudiant à modifier dans la base de données. l'dentifiant de
	 *            l'etudiant 1 ne doit pas etre null, supérieur à 0 et doit
	 *            exister dans la base de données.
	 * @param Etudiant2
	 *            qui contient les information qu'on veut modifier le nom et le
	 *            prenom de l'etudiant 2 ne doivent pas etre null.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void update(Etudiant t, Etudiant t2) throws Exception {
		if (t == null)
			throw new Exception("Etudiant null");
		if (t2 == null)
			throw new Exception("Etudiant null");
		
		if (t.getNumEtudiant() < 0)
			throw new Exception("Identifiant t1 inférieur à 0");
		
		else if (t2.getNom() == null)
			throw new Exception("Nom null");
		else if (t2.getPrenom() == null)
			throw new Exception("Prenom null");
		
		try {
			executeUpdate(
					"update FL1_Etudiant set nom = ?, prenom = ? where numEtudiant = ?",
					t2.getNom(), t2.getPrenom(), t.getNumEtudiant());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Récupération de tous les étudiants existant dans la base de données.
	 * 
	 * @return Collection<Etudiant> La liste de tous les étudiants contenues
	 *         dans la base de données.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public Collection<Etudiant> getAllEtudiants() throws Exception {
		Collection<Etudiant> etudiants = new java.util.HashSet<Etudiant>();
		
		try {
			etudiants = findBeans(
					"select numEtudiant,nom,prenom from FL1_Etudiant",
					new ResultSetToEtudiant());
			System.out.println("llll");
			
		} catch (SQLException e) {
			System.out.println("llll");
			
			throw new Exception(e.getMessage());
		}
		return etudiants;
	}
	
	/**
	 * Récupération de l'étudiant existant dans la base de données.
	 * 
	 * @param numEtudiant
	 *            ne numéro d'étudiant
	 * @return l'étudiant s'il existe.
	 * @throws Exception
	 *             Si l'étudiant n'existe pas ou si une erreur s'est produite.
	 */
	public Etudiant getEtudiant(Integer numEtudiant) throws Exception {
		Collection<Etudiant> etudiants = new java.util.HashSet<Etudiant>();
		try {
			
			etudiants = findBeans(
					"select numEtudiant,nom,prenom from FL1_Etudiant  where numEtudiant=?",
					new ResultSetToEtudiant(), numEtudiant);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return etudiants.iterator().next();
	}
	
	/**
	 * Récupération de tous les étudiants existant dans la base de données qui
	 * ont une ue.
	 * 
	 * @param ue
	 *            l'ue ou on voudrait la liste des étudiants associés
	 * @return la listes des étudiants
	 * @throws Exception
	 *             Si les étudiant n'existe pas ou si une erreur s'est produite.
	 */
	public Collection<Etudiant> etudiantsByUe(String ue) throws Exception {
		// TODO Auto-generated method stub
		Collection<Etudiant> etudiants = new java.util.HashSet<Etudiant>();
		try {
			
			etudiants = findBeans(
					"select DISTINCT e.numEtudiant,nom,prenom from FL1_Etudiant e, FL1_UesEtudiants e1  where e.numEtudiant=e1.numEtudiant and e1.nomUe=?",
					new ResultSetToEtudiant(), ue);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return etudiants;
	}
	
	/**
	 * Récupération de tous les étudiants existant dans la base de données qui
	 * ont un certain professeur dans une ue.
	 * 
	 * @param id
	 *            l'id du professeur
	 * @return la liste des étudiants
	 * @throws Exception
	 *             Si les étudiant n'existe pas ou si une erreur s'est produite.
	 */
	public Collection<Etudiant> etudiantsByProfesseur(Integer id)
			throws Exception {
		// TODO Auto-generated method stub
		Collection<Etudiant> etudiants = new java.util.HashSet<Etudiant>();
		try {
			
			etudiants = findBeans(
					"select DISTINCT e.numEtudiant,e.nom,e.prenom from FL1_Etudiant e, FL1_UesEtudiants e1, FL1_Ue p  where e.numEtudiant=e1.numEtudiant and e1.nomUe=p.nom and p.idProf=? ",
					new ResultSetToEtudiant(), id);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return etudiants;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
