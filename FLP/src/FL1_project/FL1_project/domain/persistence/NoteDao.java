package FL1_project.FL1_project.domain.persistence;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import org.junit.Test;

import FL1_project.FL1_project.domain.Note;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre Cette classe permet de récupérer/supprimer/mettre à
 *         jour des données concernant les notes.
 */
public class NoteDao extends JdbcTools implements Dao<Note, Note> {
	public static String tableNote = "CREATE TABLE IF NOT EXISTS FL1_Note("
			+ "valeur Double,"
			+ "nomUE Varchar(50) not null,"
			+ "numEtudiant Integer not null,"
			+ "CONSTRAINT numuE FOREIGN KEY (nomUE) REFERENCES FL1_Ue(nom),"
			+ "CONSTRAINT numET FOREIGN KEY (numEtudiant) REFERENCES FL1_Etudiant(numEtudiant))";
	
	public NoteDao() {
		init();
	}
	
	/**
	 * Initialisation de la table de la base de donnée de la note.
	 * 
	 * @throws SQLException
	 */
	public void initTable() throws SQLException {
		init();
		executeUpdate(tableNote);
	}
	
	@Test
	public void test() throws SQLException {
		initTable();
	}
	
	/**
	 * Supression de la note de la base de données dont l'instance est passé en
	 * paramètre.
	 * 
	 * @param NoteEtudiant
	 *            La note à supprimer de la base de données qui doit exister
	 *            dans la base de données.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	@Override
	public void remove(Note t) throws Exception {
		try {
			executeUpdate(
					"delete from FL1_Note where valeur = ? and nomUE = ? and numEtudiant = ?",
					t.getValeur(), t.getUe().getNom(), t.getEtudiant()
							.getNumEtudiant());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Insertion d'une note dans la base de données dont l'instance est passé en
	 * paramètre.
	 * 
	 * @param NoteEtudiant
	 *            La note à insérer dans la base de données, le nomUE et le
	 *            numEtudiant doivent exister dans la base de données.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void insert(Note t) throws Exception {
		try {
			executeUpdate(
					"insert into FL1_Note (valeur, nomUE, numEtudiant)  values (?, ?, ?)",
					(Double) t.getValeur(), t.getUe().getNom(), t.getEtudiant()
							.getNumEtudiant());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Modification d'une note dans la base de données dont l'instance est passé
	 * en paramètre.
	 * 
	 * @param Note
	 *            : La note à modifier dans la base de données et qui existe
	 *            dans la base de données.
	 * @param Note2
	 *            : Contient les informations de la note que l'on veut modifier,
	 *            le nomUE et le numEtudiant doivent exister dans la base de
	 *            données.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void update(Note t, Note t2) throws Exception {
		try {
			executeUpdate(
					"update FL1_Note set valeur = ? , nomUE = ? , numEtudiant = ? where nomUE = ? and numEtudiant = ?",
					t2.getValeur(), t2.getUe().getNom(), t2.getEtudiant()
							.getNumEtudiant(), t.getUe().getNom(), t
							.getEtudiant().getNumEtudiant());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Récupération de la note d'un étudiant d'une ue dans la base de donndées.
	 * 
	 * @param nomUe
	 *            le nom de l'ue dont on souhaite récupérer une note, doit
	 *            exister dans la base de données.
	 * @param numEtudiant
	 *            le nom de l'étudiant dont on souhaite récupérer une note, doit
	 *            exister dans la base de données.
	 * @return Collection<Note> Une note (récupéré de la base de données) de
	 *         l'étudiant et de l'ue passée en paramètre.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public Note getNoteByUeAndStudent(String nomUe, Integer numEtudiant)
			throws Exception {
		Collection<Note> notes = null;
		try {
			notes = findBeans(
					"select a.valeur, e.numEtudiant, e.nom as nomEtu, e.prenom as prenomEtu, a.nomUE, p.identifiant as idProf, p.nom, p.prenom "
							+ "from FL1_Note a, FL1_Etudiant e, FL1_Ue u, FL1_Professeur p where a.nomUE=? and a.numEtudiant = e.numEtudiant and a.numEtudiant=? "
							+ "and a.nomUE = u.nom and u.idProf = p.identifiant",
					new ResultSetToNote(), nomUe, numEtudiant);
			
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return notes.iterator().next();
	}
	
	public Map<String, String> getNoteByStudent(Integer numEtudiant)
			throws Exception {
		Map<String, String> notes = null;
		try {
			notes = findBeans(
					"select nomUe, valeur from FL1_Note e where e.numEtudiant=?",
					numEtudiant);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return notes;
	}
	
	public static void main(String[] args) throws Exception {
		Map<String, String> notes = new NoteDao().getNoteByStudent(1);
		System.out.println(notes);
	}
	
	/**
	 * Récupération des notes d'une ue dans la base de donndées.
	 * 
	 * @param nomUe
	 *            : le nom de l'ue dont on souhaite récupérer une note, doit
	 *            exister dans la base de données.
	 * @return Collection<Note> Une liste de notes (récupéré de la base de
	 *         données) de l'ue passée en paramètre.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public Collection<Note> getNotesUe(String nomUe) throws Exception {
		Collection<Note> notes = null;
		
		try {
			notes = findBeans(
					"select a.valeur, e.numEtudiant, e.nom as nomEtu, e.prenom as prenomEtu, a.nomUE, p.identifiant as idProf, p.nom, p.prenom "
							+ "from FL1_Note a, FL1_Etudiant e, FL1_Ue u, FL1_Professeur p where a.nomUE=? and a.numEtudiant = e.numEtudiant "
							+ "and a.nomUE = u.nom and u.idProf = p.identifiant",
					new ResultSetToNote(), nomUe);
			
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return notes;
	}
}
