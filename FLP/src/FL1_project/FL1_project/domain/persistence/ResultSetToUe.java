package FL1_project.FL1_project.domain.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.domain.UE;
import FL1_project.FL1_project.service.implantation.ProfesseurService;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette class permet de creer via un ResultSet une UE avec ses
 *         informations contenues dans le resultSet.
 */
class ResultSetToUe implements ResultSetToBean<UE> {
	
	/**
	 * Cette méthode permet de creer une instance de la class UE via les
	 * informations contenues dans java.sql.ResultSet
	 * 
	 * @param ResultSet
	 *            est le résultat d'une requette sur la base de donnée renvoyant
	 *            un/des résultats.
	 */
	@Override
	public UE toBean(ResultSet rs) throws SQLException {
		ProfesseurService service = new ProfesseurService();
		UE ue = new UE();
		ue.setNom(rs.getString(1));
		Professeur professeur = null;
		try {
			professeur = service.professeurById(rs.getInt(2));
		} catch (Exception e) {
			e.printStackTrace();
		}
		ue.setProfesseur(professeur);
		return ue;
	}
	
}