package FL1_project.FL1_project.domain.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import FL1_project.FL1_project.domain.Etudiant;
import FL1_project.FL1_project.domain.Note;
import FL1_project.FL1_project.domain.Professeur;
import FL1_project.FL1_project.domain.UE;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre Cette class permet de creer via un ResultSet une
 *         Note avec ses informations contenues dans le resultSet.
 */
class ResultSetToNote implements ResultSetToBean<Note> {
	
	/**
	 * Cette méthode permet de creer une instance de la class Note via les
	 * informations contenues dans java.sql.ResultSet
	 * 
	 * @param ResultSet
	 *            est le résultat d'une requette sur la base de donnée renvoyant
	 *            un/des résultats.
	 */
	@Override
	public Note toBean(ResultSet rs) throws SQLException {
		// TODO Auto-generated method stub
		Note note = new Note();
		note.setValeur(rs.getDouble(1));
		Etudiant etu = new Etudiant();
		etu.setNumEtudiant(rs.getInt(2));
		etu.setNom(rs.getString(3));
		etu.setPrenom(rs.getString(4));
		note.setEtudiant(etu);
		UE ue = new UE();
		ue.setNom(rs.getString(5));
		Professeur p = new Professeur();
		p.setIdentifiant(rs.getInt(6));
		p.setNom(rs.getString(7));
		p.setPrenom(rs.getString(8));
		ue.setProfesseur(p);
		note.setUe(ue);
		return note;
	}
	
	// numEtu
	
}