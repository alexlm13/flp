package FL1_project.FL1_project.domain.persistence;

public class Identification {
	String nomPilote, url, identifiant, motDePasse;
	
	public String getNomPilote() {
		return nomPilote;
	}
	
	public void setNomPilote(String nomPilote) {
		this.nomPilote = nomPilote;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getIdentifiant() {
		return identifiant;
	}
	
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	
	public String getMotDePasse() {
		return motDePasse;
	}
	
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
}
