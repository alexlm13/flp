package FL1_project.FL1_project.domain.persistence;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */
public interface Dao<T, F> {
	public void remove(F t) throws Exception;
	
	public void insert(T t) throws Exception;
	
	public void update(T t, T t2) throws Exception;
}
