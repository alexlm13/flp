package FL1_project.FL1_project.domain.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//CachedRowSetImpl;
/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Class de gestion des exécutions/réceptions des requêtes et ses
 *         résultats s'il y a.
 */
public class JdbcTools {
	private String nomPilote, url, identifiant, motDePasse;
	BasicDataSource bds;
	
	public void init() {
		AbstractApplicationContext context = new ClassPathXmlApplicationContext(
				"spring.xml");
		context.registerShutdownHook();
		setIdentifiant(context.getBean("IdentificationProgramme",
				Identification.class));
		context.close();
	}
	
	public void close() {
	}
	
	public boolean setIdentifiant(Identification id) {
		if (id.getNomPilote() == null || id.getUrl() == null
				|| id.getIdentifiant() == null || id.getMotDePasse() == null)
			return false;
		nomPilote = id.getNomPilote();
		url = id.getUrl();
		identifiant = id.getIdentifiant();
		motDePasse = id.getMotDePasse();
		
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bds = new BasicDataSource();
		bds.setUrl(nomPilote + ":" + url);
		bds.setUsername(identifiant);
		bds.setPassword(motDePasse);
		return true;
	}
	
	public Connection newConnection() throws SQLException {
		Connection c = bds.getConnection();
		return c;
	}
	
	public void quietClose(Connection c, Statement st) throws SQLException {
		if (c != null) {
			c.close();
		}
		if (st != null) {
			st.close();
		}
	}
	
	public void executeUpdate(String query) throws SQLException {
		if (nomPilote == null || url == null || identifiant == null
				|| motDePasse == null)
			throw new SQLException(
					"Identifiant de connection à la bade de données non initialisées");
		Connection c = null;
		int rs = -1;
		Statement st = null;
		try {
			init();
			c = newConnection();
			st = c.createStatement();
			st.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			quietClose(c, st);
		}
	}
	
	public int executeUpdate(String sql, java.io.Serializable... parameters)
			throws SQLException {
		if (nomPilote == null || url == null || identifiant == null
				|| motDePasse == null)
			throw new SQLException(
					"Identifiant de connection à la bade de données non initialisées");
		
		int rs = -1;
		init();
		Connection c = null;
		PreparedStatement st = null;
		try {
			c = newConnection();
			st = c.prepareStatement(sql);
			int i = 1;
			for (java.io.Serializable s : parameters) {
				if (s instanceof Integer) {
					st.setInt(i, (Integer) s);
				} else if (s instanceof Double) {
					st.setDouble(i, (Double) s);
				} else if (s instanceof Float) {
					st.setFloat(i, (Float) s);
				} else if (s instanceof String) {
					st.setString(i, (String) s);
				}
				++i;
			}
			st.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			quietClose(c, st);
		}
		return rs;
	}
	
	public <T> Collection<T> findBeans(String sql, ResultSetToBean<T> mapper,
			java.io.Serializable... parameters) throws SQLException {
		if (nomPilote == null || url == null || identifiant == null
				|| motDePasse == null)
			throw new SQLException(
					"Identifiant de connection à la bade de données non initialisées");
		int i = 1;
		Collection<T> beans = null;
		init();
		Connection c = null;
		PreparedStatement st = null;
		try {
			c = newConnection();
			st = c.prepareStatement(sql);
			for (java.io.Serializable s : parameters) {
				if (s instanceof Integer) {
					st.setInt(i, (Integer) s);
				} else if (s instanceof Double) {
					st.setDouble(i, (Double) s);
				} else if (s instanceof String) {
					st.setString(i, (String) s);
				}
				++i;
			}
			st.execute();
			beans = new HashSet<T>();
			System.out.println("jdbcTools");
			while (st.getResultSet().next()) {
				beans.add(mapper.toBean(st.getResultSet()));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			quietClose(c, st);
		}
		return beans;
		
	}
	
	public Map<String, String> findBeans(String sql,
			java.io.Serializable... parameters) throws SQLException {
		
		if (nomPilote == null || url == null || identifiant == null
				|| motDePasse == null)
			throw new SQLException(
					"Identifiant de connection à la bade de données non initialisées");
		int i = 1;
		Map<String, String> beans = new HashMap<String, String>();
		init();
		Connection c = null;
		PreparedStatement st = null;
		try {
			c = newConnection();
			st = c.prepareStatement(sql);
			for (java.io.Serializable s : parameters) {
				if (s instanceof Integer) {
					st.setInt(i, (Integer) s);
				} else if (s instanceof Double) {
					st.setDouble(i, (Double) s);
				} else if (s instanceof String) {
					st.setString(i, (String) s);
				}
				++i;
			}
			st.execute();
			while (st.getResultSet().next()) {
				beans.put(st.getResultSet().getString(1), st.getResultSet()
						.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			quietClose(c, st);
		}
		return beans;
		
	}
	
	public String getNomPilote() {
		return nomPilote;
	}
	
	public void setNomPilote(String nomPilote) {
		this.nomPilote = nomPilote;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getIdentifiant() {
		return identifiant;
	}
	
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	
	public String getMotDePasse() {
		return motDePasse;
	}
	
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	
}
