package FL1_project.FL1_project.domain.persistence;

import java.sql.SQLException;
import java.util.Collection;

import FL1_project.FL1_project.domain.Professeur;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre Cette classe permet de récupérer/supprimer/mettre à
 *         jour des données concernant les professeurs.
 */
public class ProfesseurDao extends JdbcTools implements
		Dao<Professeur, Integer> {
	public static String tableProfesseur = "CREATE TABLE IF NOT EXISTS FL1_Professeur("
			+ "identifiant Integer primary key AUTO_INCREMENT,"
			+ "nom Varchar(50) not null," + "prenom Varchar(20) not null)";
	
	public ProfesseurDao() {
		init();
	}
	
	/**
	 * Initialisation de la table de la base de donnée du professeur.
	 * 
	 * @throws SQLException
	 */
	public void initTable() throws SQLException {
		init();
		executeUpdate(tableProfesseur);
	}
	
	/**
	 * Supression du professeur de la base de données dont l'instance est passé
	 * en paramètre.
	 * 
	 * @param Professeur
	 *            Le professeur à supprimer de la base de données. le professeur
	 *            passé en paramettre ne doit pas etre null. l'identifiant du
	 *            professeur ne doit pas etre inferieur a 0 et doit exister dans
	 *            la base de données.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void remove(Integer t) throws Exception {
		if (t < 0)
			throw new Exception("Identifiant inférieur à 0");
		
		try {
			executeUpdate("delete from FL1_Professeur where identifiant = ?", t);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Insertion d'un professeur dans la base de données dont l'instance est
	 * passé en paramètre.
	 * 
	 * @param Professeur
	 *            Le professeur à insérer dans la base de données. le professeur
	 *            passé en paramettre ne doit pas etre null. le nom et prenom du
	 *            professeur ne doivent pas etre null.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void insert(Professeur t) throws Exception {
		if (t == null)
			throw new Exception("Professeur null");
		else if (t.getNom() == null)
			throw new Exception("Nom null");
		else if (t.getPrenom() == null)
			throw new Exception("Prenom null");
		try {
			executeUpdate(
					"insert into FL1_Professeur (nom,prenom)  values (?,?)",
					t.getNom(), t.getPrenom());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Modification d'un professeur dans la base de données dont l'instance est
	 * passé en paramètre.
	 * 
	 * @param Professeur1
	 *            Le professeur à modifier dans la base de données. le
	 *            Professeur1 ne doit pas etre null l'identifiant du Professeur1
	 *            ne doit pas etre inferieur a 0 et doit exister dans la base de
	 *            données.
	 * @param Professeur2
	 *            qui contient les information qu'on veut modifier. le
	 *            Professeur2 ne doit pas etre null le nom et prenon du
	 *            Professeur2 ne doivent pas etre null.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void update(Professeur t, Professeur t2) throws Exception {
		if (t == null)
			throw new Exception("Professeur null");
		if (t2 == null)
			throw new Exception("Professeur null");
		if (t.getIdentifiant() < 0)
			throw new Exception("Identifiant inférieur à 0");
		
		else if (t2.getNom() == null)
			throw new Exception("Nom null");
		else if (t2.getPrenom() == null)
			throw new Exception("Prenom null");
		try {
			executeUpdate(
					"update FL1_Professeur set nom = ? , prenom= ? where identifiant = ?",
					t2.getNom(), t2.getPrenom(), t.getIdentifiant());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Récupération de tous les professeurs existant dans la base de données.
	 * 
	 * @return Collection<Professeur> La liste de tous les professeurs contenues
	 *         dans la base de données.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public Collection<Professeur> getAllProfesseur() throws Exception {
		Collection<Professeur> professeurs = new java.util.HashSet<Professeur>();
		try {
			professeurs = findBeans(
					"select identifiant,nom,prenom from FL1_Professeur",
					new ResultSetToProfesseur());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return professeurs;
	}
	
	/**
	 * Récupération du professeur professeur existant dans la base de données.
	 * 
	 * @param integer
	 *            l'id du professeur à récupérer
	 * @return le professeur
	 * @throws Lève
	 *             une exception si une erreur s'est produite ou si le
	 *             professeur n'existe pas.
	 */
	public Professeur getProfesseur(Integer integer) throws Exception {
		// TODO Auto-generated method stub
		Collection<Professeur> professeurs = new java.util.HashSet<Professeur>();
		try {
			
			professeurs = findBeans(
					"select identifiant,nom,prenom from FL1_Professeur  where identifiant=?",
					new ResultSetToProfesseur(), integer);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return professeurs.iterator().next();
	}
	
}
