package FL1_project.FL1_project.domain.persistence;

import java.sql.SQLException;
import java.util.Collection;

import FL1_project.FL1_project.domain.Administrateur;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette classe permet de récupérer/supprimer/mettre à jour des données
 *         concernant les administrateurs.
 */
public class AdministrateurDao extends JdbcTools implements
		Dao<Administrateur, String> {
	
	public static String tableAdmin = "CREATE TABLE IF NOT EXISTS FL1_Administrateur ("
			+ "motDePasse Varchar(50) primary key)";
	
	public AdministrateurDao() {
		init();
	}
	
	/**
	 * Initialisation de la table de la base de donnée de l'Administrateur
	 * 
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void initTable() throws SQLException {
		init();
		executeUpdate(tableAdmin);
	}
	
	/**
	 * Supression de l'administrateur de la base de données dont l'instance est
	 * passé en paramètre.
	 * 
	 * @param Administrateur
	 *            L'administrateur à supprimer de la base de données.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void remove(String t) throws Exception {
		if (t == null)
			throw new Exception("Mot de pass null.");
		try {
			executeUpdate(
					"delete from FL1_Administrateur where motDePasse = ?", t);
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Insertion d'un administrateur dans la base de données dont l'instance est
	 * passé en paramètre.
	 * 
	 * @param Administrateur
	 *            L'administrateur à insérer dans la base de données.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void insert(Administrateur t) throws Exception {
		if (t == null)
			throw new Exception("Administrateur null.");
		if (t.getMotDePass() == null)
			throw new Exception("Mot de pass null.");
		try {
			executeUpdate(
					"insert into FL1_Administrateur (motDePasse)  values (?)",
					t.getMotDePass());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Modification d'un administrateur dans la base de données dont l'instance
	 * est passé en paramètre.
	 * 
	 * @param Administrateur
	 *            L'administrateur à modifier dans la base de données.
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 */
	public void update(Administrateur t, Administrateur t2) throws Exception {
		if (t == null)
			throw new Exception("Administrateur null.");
		if (t2 == null)
			throw new Exception("Administrateur null.");
		
		if (t.getMotDePass() == null)
			throw new Exception("Mot de pass null.");
		try {
			executeUpdate(
					"update FL1_Administrateur set motDePasse = ? where motDePasse = ?",
					t2.getMotDePass(), t.getMotDePass());
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * Vérification de l'existence d'un administrateur
	 * 
	 * @param paddwd
	 *            Le password de l'administrateur
	 * @throws Lève
	 *             une exception si une erreur s'est produite.
	 * @return true si le mot de pass est bien d'un administrateur, false sinon
	 */
	public boolean existAdm(String paddwd) throws Exception {
		try {
			Collection<Administrateur> adm = findBeans(
					"select motDePasse from FL1_Administrateur  where motDePasse=?",
					new ResultSetToAdministrateur(), paddwd);
			if (adm.size() != 1)
				return false;
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
		return true;
	}
}
