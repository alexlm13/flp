package FL1_project.FL1_project.domain.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import FL1_project.FL1_project.domain.Professeur;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette class permet de creer via un ResultSet un Professeur avec ses
 *         informations contenues dans le resultSet.
 */
class ResultSetToProfesseur implements ResultSetToBean<Professeur> {
	
	/**
	 * Cette méthode permet de creer une instance de la class Professeur via les
	 * informations contenues dans java.sql.ResultSet
	 * 
	 * @param ResultSet
	 *            est le résultat d'une requête sur la base de donnée renvoyant
	 *            un/des résultats.
	 */
	@Override
	public Professeur toBean(ResultSet rs) throws SQLException {
		Professeur professeur = new Professeur();
		professeur.setIdentifiant(rs.getInt(1));
		professeur.setNom(rs.getString(2));
		professeur.setPrenom(rs.getString(3));
		return professeur;
	}
	
}
