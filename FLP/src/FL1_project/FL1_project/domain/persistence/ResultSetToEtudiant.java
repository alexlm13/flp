package FL1_project.FL1_project.domain.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import FL1_project.FL1_project.domain.Etudiant;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 * 
 *         Cette class permet de creer via un ResultSet un Etudiant avec ses
 *         informations contenues dans le resultSet.
 */
class ResultSetToEtudiant implements ResultSetToBean<Etudiant> {
	
	/**
	 * Cette méthode permet de creer une instance de la class Etudiant via les
	 * informations contenues dans java.sql.ResultSet
	 * 
	 * @param ResultSet
	 *            est le résultat d'une requette sur la base de donnée renvoyant
	 *            un/des résultats.
	 */
	@Override
	public Etudiant toBean(ResultSet rs) throws SQLException {
		// System.out.println("lol");
		Etudiant etudiant = new Etudiant();
		etudiant.setNumEtudiant(rs.getInt(1));
		etudiant.setNom(rs.getString(2));
		etudiant.setPrenom(rs.getString(3));
		return etudiant;
	}
	
}