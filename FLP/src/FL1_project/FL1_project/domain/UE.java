package FL1_project.FL1_project.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public class UE implements Serializable {
	private @NonNull String nom;
	private @NonNull Professeur professeur;
	private @NonNull Collection<Etudiant> etudiants;
	private @NonNull Collection<Note> notes;
	
	public UE() {
		etudiants = new HashSet<Etudiant>();
		notes = new HashSet<Note>();
		nom = "";
		professeur = new Professeur();
	}
	
	@EnsuresNonNull(value = { "nom" })
	public String getNom() {
		return nom;
	}
	
	@RequiresNonNull(value = { "nom" })
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@EnsuresNonNull(value = { "professeur" })
	public Professeur getProfesseur() {
		return professeur;
	}
	
	@RequiresNonNull(value = { "professeur" })
	public void setProfesseur(Professeur professeur) {
		this.professeur = professeur;
	}
	
	@EnsuresNonNull(value = { "etudiants" })
	public Collection<Etudiant> getEtudiants() {
		return etudiants;
	}
	
	@RequiresNonNull(value = { "etudiants" })
	public void setEtudiants(Collection<Etudiant> etudiants) {
		this.etudiants = etudiants;
	}
	
	@EnsuresNonNull(value = { "notes" })
	public Collection<Note> getNotes() {
		return notes;
	}
	
	@RequiresNonNull(value = { "notes" })
	public void setNotes(Collection<Note> notes) {
		this.notes = notes;
	}
	
	@Override
	public String toString() {
		return "UE [nom=" + nom + "]";
	}
	
	@RequiresNonNull(value = { "etudiants" })
	public void addEtudiant(Etudiant etudiant) {
		etudiants.add(etudiant);
	}
	
	@RequiresNonNull(value = { "notes" })
	public void addNote(Note note) {
		notes.add(note);
	}
	
}
