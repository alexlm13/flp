package FL1_project.FL1_project.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public class Etudiant implements Utilisateur, Serializable {
	
	private @NonNull String nom;
	private @NonNull String prenom;
	private @NonNull String droits;
	private @NonNull int numEtudiant;
	
	public String getDroits() {
		return droits;
	}
	
	@RequiresNonNull(value = { "droits" })
	public void setDroits(String droits) {
		this.droits = droits;
	}
	
	private Collection<Note> notes;
	private Collection<UE> ues;
	
	public Etudiant() {
		notes = new HashSet<Note>();
		ues = new HashSet<UE>();
		nom = "";
		prenom = "";
		numEtudiant = 0;
		droits = "";
	}
	
	@EnsuresNonNull(value = { "notes" })
	public Collection<Note> getNotes() {
		return notes;
	}
	
	@RequiresNonNull(value = { "notes" })
	public void setNotes(Collection<Note> notes) {
		this.notes = notes;
	}
	
	@EnsuresNonNull(value = { "ues" })
	public Collection<UE> getUes() {
		return ues;
	}
	
	@RequiresNonNull(value = { "ues" })
	public void setUes(Collection<UE> ues) {
		this.ues = ues;
	}
	
	@RequiresNonNull(value = { "UE" })
	public void addUe(UE UE) {
		ues.add(UE);
	}
	
	@EnsuresNonNull(value = { "nom" })
	public String getNom() {
		return nom;
	}
	
	@RequiresNonNull(value = { "nom" })
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@EnsuresNonNull(value = { "prenom" })
	public String getPrenom() {
		return prenom;
	}
	
	@RequiresNonNull(value = { "prenom" })
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	@EnsuresNonNull(value = { "numEtudiant" })
	public int getNumEtudiant() {
		return numEtudiant;
	}
	
	@RequiresNonNull(value = { "numEtudiant" })
	public void setNumEtudiant(int numEtudiant) {
		this.numEtudiant = numEtudiant;
	}
	
	@Override
	public String toString() {
		return "Etudiant [nom=" + nom + ", prenom=" + prenom + ", numEtudiant="
				+ numEtudiant + "]";
	}
	
	public void addNote(@NonNull Note note) {
		notes.add(note);
	}
	
	public boolean equals(Etudiant e) {
		return (this.nom.equals(e.getNom())
				&& this.prenom.equals(e.getPrenom()) && this.numEtudiant == e
					.getNumEtudiant());
	}
	
	@EnsuresNonNull(value = { "droits" })
	public String droits() {
		return droits;
	}
	
}
