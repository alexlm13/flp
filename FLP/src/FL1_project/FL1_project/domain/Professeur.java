package FL1_project.FL1_project.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import org.checkerframework.checker.nullness.qual.EnsuresNonNull;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.RequiresNonNull;

/**
 * @author LOUKAH Maria
 * @author BARREAULT julien
 * @author BLANC Michaël
 * @author LEMAIRE Alexandre
 */

public class Professeur implements Utilisateur, Serializable {
	private @NonNull String nom;
	private @NonNull String prenom;
	private @NonNull String droits;
	private @NonNull int identifiant;
	@NonNull
	Collection<UE> ues;
	
	public String getDroits() {
		return droits;
	}
	
	@RequiresNonNull(value = { "droits" })
	public void setDroits(String droits) {
		this.droits = droits;
	}
	
	public Professeur() {
		ues = new HashSet<UE>();
		identifiant = 0;
		nom = "";
		prenom = "";
		droits = "";
	}
	
	@EnsuresNonNull(value = { "nom" })
	public String getNom() {
		return nom;
	}
	
	@RequiresNonNull(value = { "nom" })
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@EnsuresNonNull(value = { "prenom" })
	public String getPrenom() {
		return prenom;
	}
	
	@RequiresNonNull(value = { "prenom" })
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	@EnsuresNonNull(value = { "identifiant" })
	public int getIdentifiant() {
		return identifiant;
	}
	
	@RequiresNonNull(value = { "identifiant" })
	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}
	
	@EnsuresNonNull(value = { "ues" })
	public Collection<UE> getUes() {
		return ues;
	}
	
	@RequiresNonNull(value = { "ues" })
	public void setUes(Collection<UE> ues) {
		this.ues = ues;
	}
	
	@Override
	public String toString() {
		return "Professeur [nom=" + nom + ", prenom=" + prenom
				+ ", identifiant=" + identifiant + "]";
	}
	
	public boolean equals(Professeur p) {
		return (this.nom.equals(p.getNom())
				&& this.prenom.equals(p.getPrenom()) && this.identifiant == p
					.getIdentifiant());
	}
	
	@Override
	@EnsuresNonNull(value = { "droits" })
	public String droits() {
		return droits;
	}
}
